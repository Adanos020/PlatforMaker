About
=====
PlatforMaker is a platformer game engine allowing the user create and play their own platformer games.

Build
=====================
These instructions should work on any operating system supported by D (including Windows, OS X, and various Linux distributions).
1. Clone the repository.
2. Install a [D compiler](https://dlang.org/download.html).
3. Install [DUB](http://code.dlang.org/download) and add it to your PATH environment variable.
4. Open a command line (cmd on Windows) and navigate to the directory you cloned the repository into.
5. Enter `dub --config=platformaker-[PLATFORM] --build=[BUILD MODE]` where you should replace `[PLATFORM]` with `linux` or `windows`, and `[BUILD MODE]` with `debug` or `release`.
6. Done!

Trello
======
You can view the progress track [here](https://trello.com/b/gJMXD1Zh/platformaker "Board on trello").
