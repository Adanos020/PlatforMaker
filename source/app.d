/++
 + Program's main module. Contains game settings, the window handler, and the main
 + function.
 +
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module app;


// Program modules.
import engine.appstate,
       engine.inputhandler,
       engine.settings,
       lib.imgui,
       lib.observer,
       lib.types,
       lib.util;

// DSFML.
import dsfml.graphics;

// Phobos.
import std.conv,
       std.stdio;


private: // Classes.

class App : Observer
{
private: // Fields.

        Stack!AppState _states;
        RenderWindow _window;


private: // Methods.

        this()
        {
                Subject.add(this);

                // Loading the game config.
                Settings.load();

                // Listing all the games.
                listGames();
                
                // Initialising stuff.
                imguiInit();

                _window = new RenderWindow;
                _window.setFramerateLimit(cast(uint) Settings.Video.FPS);
                _window.setKeyRepeatEnabled(false);
        }

        void run(string title)
        {
                // Creating and configuring the window.
                _window.create(VideoMode(Settings.Video.windowSize.x, Settings.Video.windowSize.y, 32),
                               title, Settings.Video.fullscreen ? Window.Style.Fullscreen : Window.Style.Close);

                // Starting the game in Main Menu.
                Subject.notify(GameEvent.AppPushStateMenu);
                mainLoop();
        }

        protected override void listen(Message msg)
        {
                switch (msg.event) with (GameEvent)
                {
                case AppPopState:
                        popState();

                        debug writeln("-- App: State popped.");
                        break;

                case AppPushStateMenu:
                        pushState(new MenuState);
                        debug writeln("-- App: Menu state pushed.");
                        break;

                case AppPushStateLevel:
                        pushState(new LevelState);
                        debug writeln("-- App: Level state pushed.");
                        break;

                case AppPushStateHub:
                        pushState(new HubState);
                        debug writeln("-- App: Map state pushed.");
                        break;

                case AppPushStateEditor:
                        if (msg.data != "")
                        {
                                // If this message was sent then the data carried must be the index
                                // of the chosen option in the EDIT menu. This means that the game
                                // loaded has an index less by one because the first option in that
                                // menu is NEW GAME.
                                const(int) gameIndex = to!int(msg.data);
                                string gamePath = gamesList[gameIndex][1];
                                pushState(new EditorState(gamePath));
                                debug writeln("-- App: Editor state pushed. Editing game: "
                                                ~ gamesList[gameIndex][0] ~ " : " ~ gamePath);
                        }
                        else
                        {
                                pushState(new EditorState());
                                debug writeln("-- App: Editor state pushed. Editing a new game.");
                        }
                        break;

                case AppExit:
                        _window.close();
                        debug writeln("-- App: Program closed.");
                        break;

                default: break;
                }
                
                topState.listen(msg);
        }

        void mainLoop()
        {
                // Setting up the timer.
                double lag = 0.0;
                Clock timer = new Clock();

                // Starting the program's main loop.
                while (_window.isOpen)
                {
                        // If there is no state to update, close the program.
                        if (topState is null)
                        {
                                _window.close();
                                break;
                        }

                        handleInput();

                        // Updating the top state.
                        lag += timer.restart().total!"usecs" / 1.0e6;
                        while (lag >= Settings.Video.FRAME_TIME)
                        {
                                topState.update(Settings.Video.FRAME_TIME);
                                UIState.update(Settings.Video.FRAME_TIME);
                                lag -= Settings.Video.FRAME_TIME;
                        }

                        draw();
                }
        }

        void handleInput()
        {
                // Handling the window events.
                Event event;
                while (_window.pollEvent(event))
                {
                        switch (event.type) with (Event.EventType)
                        {
                        case Closed:
                                _window.close();
                                break;

                        // I leave it in a switch statement as I expect to handle more window events
                        // here in the future.

                        default:
                                handleKeyboard(event);
                                handleJoystick(event);
                                handleMouse(event, _window);
                                break;
                        }
                }
        }

        void draw()
        {
                _window.clear();
                topState.draw(_window);
                _window.display();
        }

        void pushState(AppState state)
        {
                _states.push(state);
        }

        AppState topState()
        {
                if (!_states.empty) return _states.top;
                return null;
        }

        void popState()
        {
                if (!_states.empty) _states.pop();
        }
}


private: // Functions.

void main()
{
        try
        {
                auto app = new App;
                app.run("PlatforMaker");
        }
        catch (Exception ex)
        {
                stderr.writeln("[!] Exception caught: " ~ ex.msg);
        }
}
