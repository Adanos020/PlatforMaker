/++
 + A small GUI library based on the IMGUI model. Implementation based on
 + the tutorial linked below.
 +
 + Authors: Adam Gasior
 +
 + See_Also: http://sol.gfxile.net/imgui/index.html
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module lib.imgui;


// Program modules.
import engine.settings,
       lib.animatedsprite,
       lib.math,
       lib.types;
       
// DSFML.
import dsfml.graphics;

// Phobos.
import std.algorithm,
       std.conv,
       std.file,
       std.json;

debug import std.stdio;


public: // Templates.

/++
 + Mixin template for structs containing data specific for certain frames.
 +/
mixin template FrameData()
{
        @disable this();
        static Frame frame;

        alias frame this;

        align(1):
}


public: // Structures.

/++
 + Internal window.
 +/
struct Frame
{
public: // Methods.

        /++
         + Params:
         +      rect    = Area in which the frame will be enclosed.
         +      handler = Function handling all the IMGUI components in the frame.
         +      color   = Color of the frame's background.
         +/
        this(IntRect rect, void delegate(RenderTarget) handler, Color color = UIStyle.Dialogs.fillColor)
        {
                _shape = new RectangleShape(Vector2f(rect.width, rect.height));
                _shape.position = Vector2f(rect.left, rect.top);
                _shape.fillColor = color;
                _frameHandler = handler;
        }

        /++
         + Params:
         +      pos     = Position on screen.
         +      size    = Size of the frame's surface in pixels.
         +      handler = Function handling all the IMGUI components in the frame.
         +      color   = Color of the frame's background.
         +/
        this(Vector2i pos, Vector2i size, void delegate(RenderTarget) handler,
             Color color = UIStyle.Dialogs.fillColor)
        {
                this(IntRect(pos, size), handler, color);
        }

        /++
         + Changes the function handling all the IMGUI components of the frame.
         +
         + Params:
         +      handler = Function handling all the IMGUI components in the frame.
         +/
        void frameHandler(void delegate(RenderTarget) handler) @property
        {
                _frameHandler = handler;
        }

        /++
         + Draws and handles all the IMGUI components of the frame.
         +
         + Params:
         +      target = Target to which the frame and its components are going
         +               to be drawn.
         +/
        void handle(RenderTarget target)
        {
                target.draw(_shape);
                _frameHandler(target);
        }

        Vector2i position() const @property
        {
                return _shape.position.Vector2i;
        }

        Vector2i size() @property
        {
                return _shape.size.Vector2i;
        }


private: // Fields.

        RectangleShape _shape;
        void delegate(RenderTarget) _frameHandler;
}

/++
 + Assets used only by the program's UI.
 +/
struct UIResources
{
        /// Unifont.
        static Font unifont;

        /// Buttons.
        static Texture buttons;

        /++
         + Loads the assets.
         +
         + Returns:
         +      <code>true</code> if loading succeeds, <code>false</code> otherwise.
         +/
        static bool load()
        {
                unifont = new Font;
                buttons = new Texture;

                if (!unifont.loadFromFile("ui_resources/unifont.ttf"))
                        return false;
                if (!buttons.loadFromFile("ui_resources/buttons.png"))
                        return false;
                return true;
        }
}

/++
 + Current state of the User Interface.
 +/
struct UIState
{
static:
        /// Position of the mouse cursor relative to the window.
        Vector2i mousePos = Vector2i(0, 0);

        /// Currently pressed mouse button.
        /// 
        /// 0 - None.
        /// 1 - Left.
        /// 2 - Right.
        /// 3 - Middle.
        int mouseButton = 0;

        /// By how much was the mouse wheel scrolled?
        int mouseScroll = 0;

        /// Item that the mouse currently is pointing at.
        int hotItem = 0;

        /// Item that is currently being clicked.
        int activeItem = 0;

        /// ID of the widget that has the keyboard focus.
        int keyboardItem = 0;

        /// Key that was recently pressed.
        Keyboard.Key keyEntered = Keyboard.Key.Unknown;

        /// Key modifier.
        int keyMod = 0;

        /// Character entered.
        dchar keyChar = 0;

        /// ID of the last processed component.
        int lastItem = 0;

        /// Time elapsed.
        real timeElapsed = 0;

        /// Time elapsed.
        double frameTime = 0;

        /++
         + Updates the UI state by a given time step.
         +/
        void update(double dt)
        {
                timeElapsed += dt;
                frameTime = dt;
        }
}

/++
 + Look and feel of the UI.
 +/
struct UIStyle
{
        struct Dialogs
        {
        static:
                Color fillColor;

                Color outlineColor;
                int   outilneThickness;
        }

        struct Buttons
        {
        static:
                Color    hotFillColor;
                Color    coldFillColor;

                Color    hotOutlineColor;
                Color    coldOutlineColor;
                int      outlineThickness;
                
                Color    shadowColor;
                Vector2i shadowDisplacement;

                Vector2i activeDisplacement;

                uint     fontSize;
        }

        struct ComboBoxes
        {
        static:
                Color fillColor;
                Color outlineColor;
                int   outlineThickness;

                uint  fontSize;
        }

        struct CroppableImages
        {
        static:
                Color fillColor;
                Color outlineColor;
                int   outilneThickness;
        }

        struct ListBoxes
        {
        static:
                Color boxFillColor;
                Color boxOutlineColor;
                int   boxOutlineThickness;

                Color selectionFillColor;
                Color selectionOutlineColor;
                int   selectionOutlineThickness;

                int   fontSize;
        }

        struct ScrollBars
        {
        static:
                Color boxFillColor;
                Color boxOutlineColor;
                int   boxOutlineThickness;

                Color thumbColdFillColor;
                Color thumbHotFillColor;

                Color thumbColdOutlineColor;
                Color thumbHotOutlineColor;
                int   thumbOutlineThickness;

                int   width;
                int   gap;
        }

        struct Spinners
        {
        static:
                Color boxFillColor;
                Color boxOutlineColor;
                int   boxOutlineThickness;

                uint  fontSize;
        }

        struct Tabs
        {
        static:
                Color shadowColor;
                uint  fontSize;
                int   gap;
        }

        struct TextBlocks
        {
        static:
                Color fillColor;
                Color outlineColor;
                int   outlineThickness;
        }

        struct TextFields
        {
        static:
                Color fillColor;
                Color outlineColor;
                int   outlineThickness;

                Color cursorColor;
                real  caretFlashingSpeed; // Times per second.
        }

        struct TickBoxes
        {
        static:
                Color    hotFillColor;
                Color    coldFillColor;
                Color    outlineColor;
                int      outlineThickness;

                Vector2i size;

                uint     fontSize;
        }
}

public: // Component-specific structs.

/++
 + Data for combo box.
 +/
struct ComboBoxData
{
        /// Does the combo box reveal the list?
        bool unwound = false;

        /// A combo box is nothing more than just an unwindable list box.
        ListBoxData lbdata;

        alias lbdata this;
        
        /++
         + Sets the data back to default.
         +/
        void reset()
        {
                unwound = false;
                lbdata.reset();
        }
}

/++
 +
 +/
struct CroppableImageData
{
        /// Vertical scrollbar's position.
        uint scrollV = 0;

        /// Horizontal scrollbar's position.
        uint scrollH = 0;

        /// Top left vertex of selection rectangle.
        Vector2i selStart = Vector2i(-1, -1);

        /// Bottom right vertex of selection rectangle.
        Vector2i selEnd = Vector2i(-1, -1);

        /// Zoom level.
        float zoom;

        /++
         + Sets the data back to default.
         +/
        void reset()
        {
                scrollV  = 0;
                scrollH  = 0;
                selStart = Vector2i(-1, -1);
                selEnd   = Vector2i(-1, -1);
                zoom     = 1;
        }
}

/++
 + Data for list box.
 +/
struct ListBoxData
{
        /// Currently indicated entry.
        int currEntry = -1;

        /// Vertical scrollbar's position.
        uint scrollV = 0;

        /// Horizontal scrollbar's position.
        uint scrollH = 0;

        /// List of entries.
        string[] entries;

        /++
         + Sets the data back to default.
         +/
        void reset()
        {
                entries   = [];
                currEntry = -1;
                scrollV   = 0;
                scrollH   = 0;
        }
}

/++
 +
 +/
struct TextBlockData
{
        /// Vertical scrollbar's position.
        uint scrollV = 0;

        /// Horizontal scrollbar's position.
        uint scrollH = 0;
}

public: // Enums.

/++
 + Indices of button icons.
 +/
enum ButtonIcon : uint
{
        Unknown       = 0,
        ExitDoor      = 1,
        Floppy        = 2,
        Level         = 3,
        Map           = 4,
        Pencil        = 5,
        Bricks        = 6,
        Cat           = 7,
        Multimedia    = 8,
        Wrench        = 9,
        ArrowLeft     = 10,
        ArrowRight    = 11,
        ArrowUp       = 12,
        ArrowDown     = 13,
        Plus          = 14,
        Minus         = 15,
        RedCross      = 16,
        GreenTick     = 17,
        FolderPaper   = 18,
        FolderArrowUp = 19,
        PointingHand  = 20,
}


private: // Variables.

align(1)
{
        RectangleShape _rectangle;
        RenderTexture  _renderTexture;
        Sprite         _image;
        AnimatedSprite _animage;
        Text           _text;
}


public: // Functions.

/++
 + Draws an animated image.
 +
 + Params:
 +      target    = Render target to which the box will be drawn.
 +      pos       = Position at which the box appears.
 +      scale     = Factor by which the sprite will be magnified.
 +      texture   = Spritesheet.
 +      animation = ID of the animation.
 +      play      = If true the animation will be played, if false it will be reset and stopped.
 +      color     = Color to which the sprite will be dyed.
 +/
@system
void animatedImage(RenderTarget target, Vector2i pos, Vector2f scale, in Texture texture,
                   string animation, bool play = true, Color color = Color.White)
{
        _animage.setAnimation(animation);
        _animage.setTexture(texture);
        _animage.position = pos.Vector2f;
        _animage.scale    = scale.Vector2f;
        _animage.color    = color;

        if (play) _animage.resume();
        else      _animage.stop();

        _animage.update(UIState.frameTime);

        target.draw(_image);
}

/++
 + Draws a box.
 +
 + Params:
 +      target  = Render target to which the box will be drawn.
 +      pos     = Position at which the box appears.
 +      size    = Box size.
 +      fill    = Fill color.
 +      outline = Outline color.
 +/
@system
void box(RenderTarget target, Vector2i pos, Vector2i size, Color fill,
         int outlineThickness = 1, Color outlineColor = Color.Black)
{
        _rectangle.position         = pos.Vector2f;
        _rectangle.size             = size.Vector2f;
        _rectangle.fillColor        = fill;
        _rectangle.outlineColor     = outlineColor;
        _rectangle.outlineThickness = outlineThickness;
        target.draw(_rectangle);
}

/++
 + Creates and handles a button.
 +
 + Params:
 +      target     = Render target to which the button will be drawn.
 +      area       = Rectangular area occupied by the button.
 +      whatisthis = Description of what the button does, displayed in the bottom
 +                   left corner of the window.
 +      id         = Unique ID used to determine which item is hot and/or active.
 +
 + Returns:
 +      <code>true</code> if the button is clicked, <code>false</code> otherwise.
 +/
@system
bool button(RenderTarget target, IntRect area, string whatisthis = "", int id = __LINE__)
{
        // Checking if the button is a hot item.
        if (area.regionHit)
        {
                UIState.hotItem = id;
                // If it is hot and left mouse button is pressed then set it active.
                if ((UIState.activeItem == 0) && (UIState.mouseButton == 1))
                {
                        UIState.activeItem = id;
                        UIState.keyboardItem = id;
                }

                // What is this button doing?
                target.label(Vector2i(3, Settings.Video.windowSize.y - 18), whatisthis, 15);
        }

        if (UIState.keyboardItem == 0) UIState.keyboardItem = id;

        // Drawing the shadow of the button.
        target.box(Vector2i(area.left, area.top) + UIStyle.Buttons.shadowDisplacement,
                   Vector2i(area.width, area.height), UIStyle.Buttons.shadowColor);

        if (UIState.hotItem == id)
        {
                // If the button is hot then it has to be highlighted.
                // If it's clicked then we move it slightly to simulate
                // it being physically pressed.
                if (UIState.activeItem == id)
                {
                        target.box(Vector2i(area.left, area.top) + UIStyle.Buttons.activeDisplacement,
                                   Vector2i(area.width, area.height), UIStyle.Buttons.hotFillColor,
                                   UIStyle.Buttons.outlineThickness, UIStyle.Buttons.hotOutlineColor);
                }
                else
                {
                        target.box(Vector2i(area.left,  area.top), Vector2i(area.width, area.height),
                                   UIStyle.Buttons.hotFillColor, UIStyle.Buttons.outlineThickness,
                                   UIStyle.Buttons.hotOutlineColor);
                }
        }
        else
        {
                // If the button is untouched then it'll be drawn normally.
                target.box(Vector2i(area.left,  area.top), Vector2i(area.width, area.height),
                           UIStyle.Buttons.coldFillColor, UIStyle.Buttons.outlineThickness,
                           UIStyle.Buttons.coldOutlineColor);
        }

        // If the left mouse button was pressed while pointing at the button
        // and now is released there, then the button was clicked.
        return (UIState.mouseButton == 0)  &&
               (UIState.hotItem     == id) &&
               (UIState.activeItem  == id);
}

/++
 + Creates and handles a button, draws a given string on it.
 +
 + Params:
 +      target     = Render target to which the button will be drawn.
 +      pos        = Position in which the button will be situated.
 +      text       = String that will be displayed on top of the button.
 +      whatisthis = Description of what the button does, displayed in the bottom
 +                   left corner of the window.
 +      id         = Unique ID used to determine which item is hot and/or active.
 +
 + Returns:
 +      <code>true</code> if the button is clicked, <code>false</code> otherwise.
 +/
@system
bool button(RenderTarget target, Vector2i pos, string text, string whatisthis = "", int id = __LINE__)
{
        // Determining the button bounds based on the text's bounds.
        Glyph glyph = UIResources.unifont.getGlyph('|', UIStyle.Buttons.fontSize, false);
        auto area = IntRect(pos.x, pos.y, cast(int) glyph.advance * cast(int) text.length,
                            cast(int) glyph.bounds.height);

        // Adjusting the button bounds.
        area.width  += 4;
        area.height -= 4;

        // Handling the button.
        auto pressed = button(target, area, whatisthis, id);
        
        // Drawing the text on top of the button.
        target.label(pos + ((UIState.activeItem == id) && (UIState.hotItem == id) ? Vector2i(4, 4) : Vector2i(2, 2))
                         - Vector2i(0, UIStyle.Buttons.fontSize / 3), text, UIStyle.Buttons.fontSize);

        return pressed;
}

/++
 + Creates and handles a button, draws an image of given icon on it.
 +
 + Params:
 +      target     = Render target to which the button will be drawn.
 +      pos        = Position in which the button will be situated.
 +      index      = Index of the image in the buttons texture that will be displayed
 +                   on top of the button.
 +      whatisthis = Description of what the button does, displayed in the bottom
 +                   left corner of the window.
 +      id         = Unique ID used to determine which item is hot and/or active.
 +
 + Returns:
 +      <code>true</code> if the button is clicked, <code>false</code> otherwise.
 +/
@system
bool button(RenderTarget target, Vector2i pos, int index, float scale = 1, string whatisthis = "",
            int id = __LINE__)
{
        // Determining up the image area.
        auto frame = index.subrect(Vector2i(16, 16), UIResources.buttons.getSize().Vector2i);

        // Handling the button.
        auto pressed = button(target, IntRect(pos, Vector2i(cast(int) (16 * scale),
                                                            cast(int) (16 * scale))),
                              whatisthis, id);

        // Drawing the image on top of the button.
        target.image((UIState.activeItem == id) && (UIState.hotItem == id) ? pos + Vector2i(2, 2) : pos,
                     Vector2f(scale, scale), UIResources.buttons, frame);

        return pressed;
}

/++
 + Creates and handles a button, draws an animated image on it.
 +
 + Params:
 +      target     = Render target to which the button will be drawn.
 +      pos        = Position in which the button will be situated.
 +      index      = Index of the image in the buttons texture that will be displayed
 +                   on top of the button.
 +      whatisthis = Description of what the button does, displayed in the bottom
 +                   left corner of the window.
 +      id         = Unique ID used to determine which item is hot and/or active.
 +
 + Returns:
 +      <code>true</code> if the button is clicked, <code>false</code> otherwise.
 +/
@system
bool button(RenderTarget target, Vector2i pos, in Texture spriteSheet, string animation,
            float scale = 1, string whatisthis = "", int id = __LINE__)
{
        // Handling the button.
        auto pressed = button(target, IntRect(pos, Vector2i(cast(int) (16 * scale),
                                                            cast(int) (16 * scale))),
                              whatisthis, id);

        // Drawing the image on top of the button.
        target.animatedImage((UIState.activeItem == id) ? pos + Vector2i(2, 2) : pos,
                             Vector2f(scale, scale), spriteSheet, animation, UIState.hotItem == id);

        return pressed;
}


/++
 + Unwindable list of options.
 +
 + Params:
 +      target     = Render target to which the combo box will be drawn.
 +      pos        = Position on screen.
 +      size       = Total maximum size. Width is always the same, height is applied when the box is unwinded.
 +      entries    = List of entries.
 +      currEntry  = Index of currently selected entry.
 +      whatisthis = Description of what the combo box does, displayed in the bottom
 +                   left corner of the window.
 +      id         = Unique ID used to determine which item is hot and/or active.
 +
 + Returns:
 +      <code>true</code> if the current option was changed, <code>false</code> otherwise.
 +/
@system
bool comboBox(RenderTarget target, Vector2i pos, Vector2i size, ref ComboBoxData data, string defaultText = "",
              string whatisthis = "", int id = __LINE__)
{
        const glyph = UIResources.unifont.getGlyph('|', UIStyle.ComboBoxes.fontSize, false);
        const cheight = glyph.bounds.height;
        bool  changed = false;

        // Determining whether the item is hot and/or active.
        if (regionHit(pos, Vector2i(size.x, cheight)))
        {
                UIState.hotItem = id;

                // If it is hot and left mouse button is pressed then set it active.
                if ((UIState.activeItem == 0) && (UIState.mouseButton == 1))
                {
                        UIState.activeItem = id;
                        UIState.keyboardItem = id;
                }

                // What is this combo box doing?
                target.label(Vector2i(3, Settings.Video.windowSize.y - 18), whatisthis, 15);
        }

        // Drawing the box.
        target.box(pos, Vector2i(size.x, cheight), UIStyle.ComboBoxes.fillColor, UIStyle.ComboBoxes.outlineThickness,
                   UIStyle.ComboBoxes.outlineColor);

        // If no entry is selected, we display the default text.
        if (data.lbdata.currEntry < 0)
        {
                target.label(pos - Vector2i(0, UIStyle.ComboBoxes.fontSize / 6), defaultText,
                             UIStyle.ComboBoxes.fontSize);
        }
        else
        {
                target.label(pos - Vector2i(0, UIStyle.ComboBoxes.fontSize / 6),
                             data.lbdata.entries[data.lbdata.currEntry], UIStyle.ComboBoxes.fontSize);
        }

        // Drawing an appropriate icon to the right of the box (wind/unwind).
        if (data.unwound)
        {
                target.image(pos + Vector2i(size.x - 16, 0), Vector2f(1, 1), UIResources.buttons,
                             ButtonIcon.ArrowUp.subrect(Vector2i(16, 16), UIResources.buttons.getSize().Vector2i));

                // Also we can draw the list box here.
                int lheight = min(size.y, cheight * cast(int) data.lbdata.entries.length);
                changed = target.listBox(pos + Vector2i(0, cheight + 1), Vector2i(size.x, lheight), data.lbdata, id + 1);
                
                // Rewinding the list back.
                if (changed) data.unwound = false;
        }
        else
        {
                target.image(pos + Vector2i(size.x - 16, 0), Vector2f(1, 1), UIResources.buttons,
                             ButtonIcon.ArrowDown.subrect(Vector2i(16, 16), UIResources.buttons.getSize().Vector2i));
        }

        // If clicked we can wind or unwind the combo box.
        if ((UIState.activeItem == id) && (UIState.mouseButton == 0)) data.unwound = !data.unwound;

        return changed;
}

/++
 +
 +/
@system
bool croppableImage(RenderTarget target, Vector2i pos, Vector2i size, in Texture texture, ref CroppableImageData data,
                    int id = __LINE__)
{
        // Setting up the box.
        target.box(pos, size, UIStyle.CroppableImages.fillColor, UIStyle.CroppableImages.outilneThickness,
                   UIStyle.CroppableImages.outlineColor);

        // Determining the size of image drawn.
        auto ipos = pos + size / 2 - texture.getSize() / 2;
        auto isiz = texture.getSize().Vector2i / data.zoom;

        // Drawing the image.
        target.image(ipos, Vector2f(data.zoom, data.zoom), texture,
                     IntRect(Vector2i(data.scrollH, data.scrollV), isiz));

        return false;
}

/++
 + Draws an image.
 +
 + Params:
 +      target  = Render target to which the image will be drawn.
 +      pos     = Position at which the image appears.
 +      texture = Texture from which an image will be composed.
 +      frame   = Fragment of the tecture that will be drawn in the image. If it's
 +                a zero rectangle then the whole area of the texture will be the frame.
 +      scale   = Factor by which the image will be magnified.
 +      color   = Color to which the image will be dyed.
 +/
@system
void image(RenderTarget target, Vector2i pos, Vector2f scale, in Texture texture,
           IntRect frame = IntRect(0, 0, 0, 0), Color color = Color.White)
{
        _image.setTexture(texture);
        _image.position = pos.Vector2f;
        _image.scale    = scale.Vector2f;
        _image.color    = color;

        if (frame == IntRect(0, 0, 0, 0))
                _image.textureRect = IntRect(0, 0, texture.getSize().x, texture.getSize().y);
        else
                _image.textureRect = frame;

        target.draw(_image);
}

/++
 + Displays text.
 +
 + Params:
 +      target   = Render target to which the text will be drawn.
 +      txt      = Text to display.
 +      pos      = Position at which the label appears.
 +      charSize = Font size.
 +      color    = Font color.
 +/
@system
void label(RenderTarget target, Vector2i pos, string txt, uint charSize = 30,
           Color color = Color.White)
{
        _text.position = pos.Vector2f;
        _text.setString(txt);
        _text.setCharacterSize(charSize);
        _text.setColor(color);
        target.draw(_text);
}

/++
 + Displays a list of entries and allows the user to select one of them.
 +
 + Params:
 +      target = Render target to which the list box will be drawn.
 +      pos    = Position on screen.
 +      size   = Size of the box.
 +      data   = ListBoxData object, containing information like current scrolls,
 +               entry list, and current entry index.
 +      id     = Unique ID used to determine which item is hot and/or active.
 +
 + Returns:
 +      <code>true</code> if the list box was clicked, <code>false</code> otherwise.
 +/
@system
bool listBox(RenderTarget target, Vector2i pos, Vector2i size, ref ListBoxData data, int id = __LINE__)
{
        // Determining the bounds of the whole list.
        const Glyph glyph = UIResources.unifont.getGlyph('|', UIStyle.ListBoxes.fontSize, false);
        int cheight = glyph.bounds.height;
        int chwidth = glyph.advance;

        int maxWidth = {
                int max = 0;
                foreach (entry; data.entries)
                {
                        int width = cast(int) entry.length * chwidth;
                        if (width > max) max = width;
                }
                return max;
        }();
        Vector2i listSize = Vector2i(max(size.x, maxWidth), 
                                     max(size.y, cheight * cast(int) data.entries.length));

        // Resizing the box if the list is greater than it.
        bool scrollH = false;
        bool scrollV = false;
        const sw = (UIStyle.ScrollBars.width + 1);
        if (listSize.x > size.x) { size.y -= sw; listSize.y -= sw; scrollH = true; }
        if (listSize.y > size.y) { size.x -= sw; listSize.x -= sw; scrollV = true; }

        // Determining whether the item is hot or active.
        if (regionHit(pos, size))
        {
                UIState.hotItem = id;
                if ((UIState.activeItem == 0) && (UIState.mouseButton == 1))
                {
                        UIState.activeItem = id;
                        UIState.keyboardItem = id;
                }

                // Handling mouse wheel.
                const displacement = UIState.mouseScroll * 50;
                /**/ if (cast(int) data.scrollV - displacement < 0)
                        data.scrollV = 0;
                else if (cast(int) data.scrollV - displacement > listSize.y - size.y)
                        data.scrollV = listSize.y - size.y;
                else    data.scrollV -= displacement;
        }

        // Drawing the box.
        target.box(pos, size, UIStyle.ListBoxes.boxFillColor, UIStyle.ListBoxes.boxOutlineThickness,
                   UIStyle.ListBoxes.boxOutlineColor);

        // Inserting ScrollBars if necessary.
        if (scrollH) target.scrollBarH(pos + Vector2i(0, size.y + 1), size.x, listSize.x - size.x, data.scrollH, id + 1);
        if (scrollV) target.scrollBarV(pos + Vector2i(size.x + 1, 0), size.y, listSize.y - size.y, data.scrollV, id + 2);
        if (scrollH && scrollV)
        {
                target.box(pos + size + Vector2i(1, 1), Vector2i(sw - 1, sw - 1), UIStyle.ScrollBars.boxFillColor,
                           UIStyle.ScrollBars.boxOutlineThickness, UIStyle.ScrollBars.boxOutlineColor);
        }

        // No need to create the render texture if there are no entries.
        if (data.entries.length == 0) return false;

        // Preparing the render texture.
        _renderTexture.create(listSize.x, listSize.y);
        _renderTexture.clear(Color.Transparent);

        // Highlight the selected entry.
        if (data.currEntry >= 0)
        {
                _renderTexture.box(Vector2i(0, cheight * data.currEntry), Vector2i(listSize.x, cheight),
                                   UIStyle.ListBoxes.selectionFillColor, UIStyle.ListBoxes.selectionOutlineThickness,
                                   UIStyle.ListBoxes.selectionOutlineColor);
        }

        bool switched = false;

        // Displaying all entries.
        foreach (int i, entry; data.entries)
        {
                _renderTexture.label(Vector2i(1, cheight * i - 2), entry.to!string, UIStyle.ListBoxes.fontSize);

                // Selecting clicked entry.
                if ((i != data.currEntry) && (UIState.activeItem == id) &&
                    regionHit(Vector2i(0, cheight * i - data.scrollV) + pos, Vector2i(size.x, cheight)))
                {
                        data.currEntry = i;
                        switched = true;
                }
        }

        // Drawing the list.
        _renderTexture.display();
        target.image(pos, Vector2f(1, 1), _renderTexture.getTexture(),
                     IntRect(Vector2i(data.scrollH, data.scrollV), size));

        // Was the selection changed?
        return switched;
}


/++
 + Params:
 +      horizontal = Is the scroll bar horizontal?
 +      target     = Render target to which the scroll bar will be drawn.
 +      pos        = Position on screen.
 +      length     = Length of the scroll bar in pixels.
 +      max        = Maximal value.
 +      value      = Reference to the variable that will be manipulated.
 +      id         = Unique ID used to determine which item is hot and/or active.
 +      
 + Returns:
 +      <code>true</code> if the scroll bar is being clicked, <code>false</code> otherwise.
 +/
@system
bool scrollBar(bool horizontal)(RenderTarget target, Vector2i pos, int length, int max,
                                ref uint value, int id = __LINE__)
{
        // How much the value has to change when the thumb is moved
        // by 1 pixel in either way?
        double ratio = cast(double) (length - 8) / max;
        if (ratio > 1) ratio = 1;

        // Thumb length.
        int tlength = length - max - 8;
        if (tlength < 5) tlength = 5;

        // Adjusting thumb's bounds.
        static if (horizontal) auto tsize = Vector2i(tlength, UIStyle.ScrollBars.width - 2 * UIStyle.ScrollBars.gap);
        else                   auto tsize = Vector2i(UIStyle.ScrollBars.width - 2 * UIStyle.ScrollBars.gap, tlength);

        // Gap between thumb and box.
        const gap = length - 2 * UIStyle.ScrollBars.gap - tlength;

        static if (horizontal)
        {
                const tpos = Vector2f(UIStyle.ScrollBars.gap + value * gap / max, UIStyle.ScrollBars.gap) + pos;
                auto  size = Vector2f(length, UIStyle.ScrollBars.width);
        }
        else
        {
                const tpos = Vector2f(UIStyle.ScrollBars.gap, UIStyle.ScrollBars.gap + value * gap / max) + pos;
                auto  size = Vector2f(UIStyle.ScrollBars.width, length);
        }

        // Determining whether the slider is hot and/or active.
        if (regionHit(pos + Vector2i(UIStyle.ScrollBars.gap, UIStyle.ScrollBars.gap),
                      size.Vector2i - Vector2i(2 * UIStyle.ScrollBars.gap, 2 * UIStyle.ScrollBars.gap)))
        {
                UIState.hotItem = id;
                if ((UIState.activeItem == 0) && (UIState.mouseButton == 1))
                {
                        UIState.activeItem = id;
                        UIState.keyboardItem = id;
                }
        }

        // Drawing the slider's body.
        target.box(pos, size.Vector2i, UIStyle.ScrollBars.boxFillColor,
                   UIStyle.ScrollBars.boxOutlineThickness, UIStyle.ScrollBars.boxOutlineColor);

        // Drawing the thumb's body.
        if ((UIState.activeItem == id) || (UIState.hotItem == id))
        {
                target.box(tpos.Vector2i, tsize, UIStyle.ScrollBars.thumbHotFillColor,
                           UIStyle.ScrollBars.thumbOutlineThickness, UIStyle.ScrollBars.thumbHotOutlineColor);
        }
        else
        {
                target.box(tpos.Vector2i, tsize, UIStyle.ScrollBars.thumbColdFillColor,
                           UIStyle.ScrollBars.thumbOutlineThickness, UIStyle.ScrollBars.thumbColdOutlineColor);
        }

        // Setting the indicated value.
        if (UIState.activeItem == id)
        {
                static if (horizontal)
                        int mousePos = UIState.mousePos.x - (UIStyle.ScrollBars.gap + pos.x + tsize.x / 2);
                else
                        int mousePos = UIState.mousePos.y - (UIStyle.ScrollBars.gap + pos.y + tsize.y / 2);

                /**/ if (mousePos < 0)   mousePos = 0;
                else if (mousePos > gap) mousePos = gap;

                static if (horizontal) const v = mousePos / (cast(double) gap / max);
                else                   const v = mousePos / (cast(double) gap / max);

                if (v != value)
                {
                        value = cast(uint) v;
                        return true;
                }
        }
        return false;
}

/++
 + Vertical scroll bar.
 +
 + Params:
 +      target = Render target to which the scroll bar will be drawn.
 +      pos    = Position on screen.
 +      length = Length of the scroll bar in pixels.
 +      max    = Maximal value.
 +      value  = Reference to the variable that will be manipulated.
 +      id     = Unique ID used to determine which item is hot and/or active.
 +      
 + Returns:
 +      <code>true</code> if the scroll bar is being clicked, <code>false</code> otherwise.
 +/
alias scrollBarV = scrollBar!false;

/++
 + Horizontal scroll bar.
 +
 + Params:
 +      target = Render target to which the scroll bar will be drawn.
 +      pos    = Position on screen.
 +      length = Length of the scroll bar in pixels.
 +      max    = Maximal value.
 +      value  = Reference to the variable that will be manipulated.
 +      id     = Unique ID used to determine which item is hot and/or active.
 +      
 + Returns:
 +      <code>true</code> if the scroll bar is being clicked, <code>false</code> otherwise.
 +/
alias scrollBarH = scrollBar!true;

/++
 +
 +/
@system
bool spinner(string buttons)(RenderTarget target, Vector2i pos, Vector2i size, ref int value,
                             int min, int max, int id = __LINE__)
{
        // Drawing the box.
        target.box(pos, size, UIStyle.Spinners.boxFillColor, UIStyle.Spinners.boxOutlineThickness,
                   UIStyle.Spinners.boxOutlineColor);

        target.label(pos, value.to!string, UIStyle.Spinners.fontSize);

        bool changed = false;



        return changed;
}

/++
 + ditto
 +/
alias spinnerMP = spinner!"-+";

/++
 + ditto
 +/
alias spinnerLR = spinner!"<>";

/++
 + ditto
 +/
alias spinnerDU = spinner!"∨∧";

/++
 + Displays a set of tabs on top of a frame and allows switching between them.
 +
 + Params:
 +      target  = Render target to which the list box will be drawn.
 +      frame  = Frame on top of which the tab switches appear.
 +      tabs    = List of tab titles to display.
 +      currTab = Index of currently selected tab.
 +      id      = Unique ID used to determine which item is hot and/or active.
 +
 + Returns:
 +      <code>true</code> if a tab was switched, <code>false</code> otherwise.
 +/
@system
bool tabs(RenderTarget target, Frame frame, string[] tabs, ref ubyte currTab, int id = __LINE__)
{
        auto pos = frame.position;
        int totalWidth = 0;

        // Establishing the areas where the tab switches appear.
        auto areas = {
                IntRect[] rects;

                foreach (i, str; tabs)
                {
                        // Determining the switch's bounds based on the text's bounds.
                        _text.setString(str);
                        _text.position = Vector2f((i == 0) ? pos.x : rects[i - 1].left + rects[i - 1].width,
                                                  pos.y);
                        _text.setCharacterSize(UIStyle.Tabs.fontSize);
                        auto tbounds = _text.getGlobalBounds();
                        auto rect = IntRect(cast(int) tbounds.left,  cast(int) tbounds.top,
                                            cast(int) tbounds.width, cast(int) tbounds.height);

                        // Adjusting the switch's bounds.
                        rect.left   -= 1;
                        rect.top    -= 7;
                        rect.width  += 14;
                        rect.height += 4;

                        rects ~= rect;
                        totalWidth += rect.width;
                }

                return rects;
        }();

        // Determining whether the item is hot or active.
        if (IntRect(pos.x, pos.y + 10, totalWidth, areas[0].height).regionHit)
        {
                UIState.hotItem = id;
                if ((UIState.activeItem == 0) && (UIState.mouseButton == 1))
                {
                        UIState.activeItem = id;
                        UIState.keyboardItem = id;
                }
        }

        // Drawing a shadowy background.
        target.box(frame.position, Vector2i(frame.size.x, UIStyle.Tabs.gap + areas[0].height),
                   UIStyle.Tabs.shadowColor);

        bool changed = false;
        foreach (ubyte i, area; areas)
        {
                // Selecting the tab entry.
                if ((i != currTab) && area.regionHit && (UIState.activeItem == id))
                {
                        currTab = i;
                        changed = true;
                }

                // Drawing all the tab switches.
                if (i == currTab)
                {
                        target.box(Vector2i((i + 1) * UIStyle.Tabs.gap + area.left, area.top),
                                   Vector2i(area.width, area.height), UIStyle.Dialogs.fillColor);
                        target.box(Vector2i((i + 1) * UIStyle.Tabs.gap + area.left, area.top + area.height),
                                   Vector2i(area.width, 1), UIStyle.Dialogs.fillColor, 0);
                }
                else
                {
                        target.box(Vector2i((i + 1) * UIStyle.Tabs.gap + area.left, area.top),
                                   Vector2i(area.width, area.height), UIStyle.Tabs.shadowColor);
                }
                target.label(Vector2i((i + 1) * UIStyle.Tabs.gap + area.left + 5, area.top - 8), tabs[i],
                             UIStyle.Tabs.fontSize);
        }

        return changed;
}

/++
 + Displays a block of text without allowing to edit it.
 +
 + Params:
 +      target   = Render target to which the list box will be drawn.
 +      pos      = Position on screen.
 +      txt      = Reference to the string that will be displayed in the text block.
 +      charSize = Size of a single character in pixels.
 +      data     = TextBlockData object, containing information about current scrolls.
 +      id       = Unique ID used to determine which item is hot and/or active.
 +/
void textBlock(RenderTarget target, Vector2i pos, Vector2i size, string txt, uint charSize, ref TextBlockData data,
               int id = __LINE__)
{
        // Getting the text bounds.
        _text.setString(txt);
        _text.setCharacterSize(charSize);
        auto tsize = Vector2i(cast(int) _text.getGlobalBounds().width, cast(int) _text.getGlobalBounds().height);

        int sw = UIStyle.ScrollBars.width + 1;
        
        // Should there be scroll bars?
        bool scrollV = false;
        bool scrollH = false;

        if (tsize.x > size.x) { size.x -= sw; scrollH = true; }
        if (tsize.y > size.y) { size.y -= sw; scrollV = true; }

        // Drawing the box.
        target.box(pos, size, UIStyle.TextBlocks.fillColor, UIStyle.TextBlocks.outlineThickness,
                   UIStyle.TextBlocks.outlineColor);

        // We don't want to create a 0-sized render texture, do we?
        if (tsize.x <= 0 || tsize.y <= 0) return;

        // Setting up the scroll bars.
        if (scrollV) target.scrollBarV(pos + Vector2i(size.x + 1, 0), size.y, tsize.y - size.y, data.scrollV, id + 1);
        if (scrollH) target.scrollBarH(pos + Vector2i(0, size.y + 1), size.x, tsize.x - size.x, data.scrollH, id + 2);
        if (scrollV && scrollH)
        {
                target.box(pos + size + Vector2i(1, 1), Vector2i(sw - 1, sw - 1), UIStyle.ScrollBars.boxFillColor,
                           UIStyle.ScrollBars.boxOutlineThickness, UIStyle.ScrollBars.boxOutlineColor);
        }

        // Rendering the text.
        _renderTexture.create(tsize.x, tsize.y);
        _renderTexture.clear(Color.Transparent);
        _renderTexture.label(Vector2i(0, -cast(int) (cast(double) charSize / 6)), txt, charSize);
        _renderTexture.display();

        target.image(pos, Vector2f(1, 1), _renderTexture.getTexture(),
                     IntRect(data.scrollH, data.scrollV, size.x, size.y));
}

/++
 + Params:
 +      target     = Render target to which the text field will be drawn.
 +      pos        = Position on screen.
 +      txt        = Reference to the string that will be edited with the text field.
 +      charSize   = Size of a single character in pixels.
 +      whatisthis = Description of what the text field edits, displayed in the bottom
 +                   left corner of the window.
 +      id         = Unique ID used to determine which item is hot and/or active.
 +
 + Returns:
 +      <code>true</code> if the text was changed, <code>false</code> otherwise.
 +/
@system
bool textField(RenderTarget target, Vector2i pos, ref string txt, int width,
               int charSize = 30, string whatisthis = "", int id = __LINE__)
{
        bool changed = false;

        // Checking if the text field is hot or active.
        if (regionHit(pos, Vector2i(width + 2, charSize)))
        {
                UIState.hotItem = id;
                if ((UIState.activeItem == 0) && (UIState.mouseButton == 1))
                {
                        UIState.activeItem = id;
                        UIState.keyboardItem = id;
                }
                // What is this textfield for?
                target.label(Vector2i(3, Settings.Video.windowSize.y - 18), whatisthis, 15);
        }

        // Drawing the field.
        target.box(pos, Vector2i(width, charSize), UIStyle.TextFields.fillColor,
                   UIStyle.TextFields.outlineThickness, UIStyle.TextFields.outlineColor);

        // Drawing the text.
        string dispTxt = txt;
        while (true)
        {
                _text.setString(dispTxt);
                _text.setCharacterSize(charSize);
                if (_text.getGlobalBounds().width < width) break;
                dispTxt = "…" ~ dispTxt[4 .. $];
        }
        target.label(pos - Vector2i(0, charSize / 5), dispTxt, charSize);

        // Drawing the cursor if the field has focus.
        if (UIState.keyboardItem == id)
        {
                auto cursorPos = Vector2i(cast(int) _text.getGlobalBounds().width + pos.x - 3,
                                          pos.y - charSize / 5);

                if (cast(int) (UIState.timeElapsed * UIStyle.TextFields.caretFlashingSpeed) % 2 == 0)
                        target.label(cursorPos, "|", charSize, UIStyle.TextFields.cursorColor);

                // Processing the keyboard input.
                if ((UIState.keyChar >= 32) && (UIState.keyChar != '\u007f'))
                {
                        txt ~= UIState.keyChar;
                        changed = true;
                }
                else if ((UIState.keyChar == '\b') && (txt.length > 0))
                {
                        --txt.length;
                }
                else if (UIState.keyChar == '\u007f')
                {
                        // TODO: handle moving cursor and Delete key.
                }
        }

        return changed;
}


/++
 + Clickable item switching between values of a boolean variable.
 +
 + Params:
 +      target     = Render target to which the text field will be drawn.
 +      pos        = Position on screen.
 +      caption    = Text displated next to the box.
 +      value      = Reference to the boolean variable to switch.
 +      whatisthis = Description of what the text field edits, displayed in the bottom
 +                   left corner of the window.
 +      id         = Unique ID used to determine which item is hot and/or active.
 +
 + Returns:
 +      <code>true</code> if the value was changed, <code>false</code> otherwise.
 +/
@system
bool tickBox(RenderTarget target, Vector2i pos, string caption, ref bool value, string whatisthis = "",
             int id = __LINE__)
{
        // Determining whether the item is hot and/or active.
        if (regionHit(pos, UIStyle.TickBoxes.size))
        {
                UIState.hotItem = id;
                if ((UIState.activeItem == 0) && (UIState.mouseButton == 1))
                {
                        UIState.activeItem = id;
                        UIState.keyboardItem = id;
                }

                // What does this tick box do?
                target.label(Vector2i(3, Settings.Video.windowSize.y - 18), whatisthis, 15);
        }

        // Was the tick box ticked?
        bool changed = false;
        if (UIState.hotItem == id)
        {
                target.box(pos, UIStyle.TickBoxes.size, UIStyle.TickBoxes.hotFillColor,
                           UIStyle.TickBoxes.outlineThickness, UIStyle.TickBoxes.outlineColor);
                if ((UIState.activeItem == id) && (UIState.mouseButton == 0))
                {
                        value = !value;
                        changed = true;
                }
        }
        else
        {
                target.box(pos, UIStyle.TickBoxes.size, UIStyle.TickBoxes.coldFillColor,
                           UIStyle.TickBoxes.outlineThickness, UIStyle.TickBoxes.outlineColor);
        }

        // Drawing the tick sign when appropriate.
        if (value)
        {
                target.image(pos, Vector2f(1, 1), UIResources.buttons,
                             15.subrect(Vector2i(16, 16), UIResources.buttons.getSize().Vector2i));
        }

        // Drawing the capion.
        target.label(pos + Vector2i(UIStyle.TickBoxes.size.x + cast(int) UIStyle.TickBoxes.fontSize / 6,
                                    -cast(int) UIStyle.TickBoxes.fontSize / 6), caption,
                     UIStyle.TickBoxes.fontSize);

        return changed;
}


/++
 + Adds a new animation.
 +
 + Params:
 +      id        = ID of the animation.
 +      animation = Animation object.
 +/
nothrow @safe
void imguiAddAnimation(string id, Animation animation)
{
        _animage.addAnimation(id, animation);
}

/++
 + Returns:
 +      <code>true</code> if IMGUI has the animation of the given ID, <code>false</code> otherwise.
 +/
nothrow
bool imguiHasAnimation(string id)
{
        return _animage.animationIDs.canFind(id);
}

/++
 + Initialising the module fields.
 +/
@system
void imguiInit()
{
        // Loading the UI resources.
        if (!UIResources.load()) throw new Exception("[!] UI resources could not be loaded.");

        // Initialising the UI's graphical objects.
        _image         = new Sprite(UIResources.buttons);
        _text          = new Text("", UIResources.unifont);
        _rectangle     = new RectangleShape;
        _renderTexture = new RenderTexture;
        _renderTexture.create(Settings.Video.windowSize.x, Settings.Video.windowSize.y);

        // Setting up the UI style.
        UIStyle.Dialogs.fillColor        = Color(0x80, 0x80, 0x80);
        UIStyle.Dialogs.outlineColor     = Color.Black;
        UIStyle.Dialogs.outilneThickness = 1;

        UIStyle.Buttons.hotFillColor       = Color(0xcc, 0xcc, 0xcc);
        UIStyle.Buttons.coldFillColor      = Color(0xaa, 0xaa, 0xaa);
        UIStyle.Buttons.hotOutlineColor    = Color.Black;
        UIStyle.Buttons.coldOutlineColor   = Color.Black;
        UIStyle.Buttons.outlineThickness   = 1;
        UIStyle.Buttons.shadowColor        = Color(0, 0, 0, 0x88);
        UIStyle.Buttons.shadowDisplacement = Vector2i(3, 3);
        UIStyle.Buttons.activeDisplacement = Vector2i(2, 2);
        UIStyle.Buttons.fontSize           = 30;

        UIStyle.ComboBoxes.fillColor        = Color(0xaa, 0xaa, 0xaa);
        UIStyle.ComboBoxes.outlineColor     = Color.Black;
        UIStyle.ComboBoxes.outlineThickness = 1;
        UIStyle.ComboBoxes.fontSize         = 15;

        UIStyle.CroppableImages.fillColor        = Color(0xaa, 0xaa, 0xaa);
        UIStyle.CroppableImages.outlineColor     = Color.Black;
        UIStyle.CroppableImages.outilneThickness = 1;

        UIStyle.ListBoxes.boxFillColor              = Color(0xaa, 0xaa, 0xaa);
        UIStyle.ListBoxes.boxOutlineColor           = Color.Black;
        UIStyle.ListBoxes.boxOutlineThickness       = 1;
        UIStyle.ListBoxes.selectionFillColor        = Color(0xff, 0x00, 0x00, 0x88);
        UIStyle.ListBoxes.selectionOutlineColor     = Color.Black;
        UIStyle.ListBoxes.selectionOutlineThickness = 1;
        UIStyle.ListBoxes.fontSize                  = 15;

        UIStyle.ScrollBars.boxFillColor          = Color(0xaa, 0xaa, 0xaa);
        UIStyle.ScrollBars.boxOutlineColor       = Color.Black;
        UIStyle.ScrollBars.boxOutlineThickness   = 1;
        UIStyle.ScrollBars.thumbColdFillColor    = Color(0xaa, 0xaa, 0xaa);
        UIStyle.ScrollBars.thumbHotFillColor     = Color.White;
        UIStyle.ScrollBars.thumbColdOutlineColor = Color.Black;
        UIStyle.ScrollBars.thumbHotOutlineColor  = Color.Black;
        UIStyle.ScrollBars.thumbOutlineThickness = 1;
        UIStyle.ScrollBars.width                 = 16;
        UIStyle.ScrollBars.gap                   = 3;

        UIStyle.Spinners.boxFillColor        = Color(0xaa, 0xaa, 0xaa);
        UIStyle.Spinners.boxOutlineColor     = Color.Black;
        UIStyle.Spinners.boxOutlineThickness = 1;
        UIStyle.Spinners.fontSize            = 30;

        UIStyle.Tabs.shadowColor = Color(0x44, 0x44, 0x44);
        UIStyle.Tabs.fontSize    = 30;
        UIStyle.Tabs.gap         = 3;

        UIStyle.TextBlocks.fillColor        = Color(0xaa, 0xaa, 0xaa);
        UIStyle.TextBlocks.outlineColor     = Color.Black;
        UIStyle.TextBlocks.outlineThickness = 1;

        UIStyle.TextFields.fillColor          = Color(0xaa, 0xaa, 0xaa);
        UIStyle.TextFields.outlineColor       = Color.Black;
        UIStyle.TextFields.outlineThickness   = 1;
        UIStyle.TextFields.cursorColor        = Color.Red;
        UIStyle.TextFields.caretFlashingSpeed = 1.5;

        UIStyle.TickBoxes.hotFillColor     = Color(0xcc, 0xcc, 0xcc);
        UIStyle.TickBoxes.coldFillColor    = Color(0xaa, 0xaa, 0xaa);
        UIStyle.TickBoxes.outlineColor     = Color.Black;
        UIStyle.TickBoxes.outlineThickness = 1;
        UIStyle.TickBoxes.size             = Vector2i(16, 16);
        UIStyle.TickBoxes.fontSize         = 15;
}

/++
 + Sets up the UI state.
 +/
nothrow @safe
void imguiPrepare()
{
        UIState.hotItem = 0;
}

/++
 + Resets the UI state.
 +/
nothrow @safe
void imguiFinish()
{
        /**/ if (UIState.mouseButton == 0)
                UIState.activeItem = 0;
        else if (UIState.activeItem == 0)
                UIState.activeItem = -1;

        UIState.keyEntered = Keyboard.Key.Unknown;
        UIState.keyChar = 0;
        UIState.mouseScroll = 0;
}


private: // Functions.

/++
 + Checks if the mouse cursor is in a given area.
 +
 + Params:
 +      rect = Area in which the mouse cursor is being detected.
 +
 + Returns:
 +      <code>true</code> if mouse cursor is detected in the area,
 +      <code>false</code> otherwise.
 +/
nothrow @safe
bool regionHit(IntRect rect)
{
        return rect.contains(UIState.mousePos);
}

/++
 + Checks if the mouse cursor is in a given area.
 +
 + Params:
 +      pos  = Position of the area in which the mouse cursor is being detected.
 +      size = Size of the area in which the mouse cursor is being detected.
 +
 + Returns:
 +      <code>true</code> if mouse cursor is detected in the area,
 +      <code>false</code> otherwise.
 +/
nothrow @safe
bool regionHit(Vector2i pos, Vector2i size)
{
        return regionHit(IntRect(pos, size));
}
