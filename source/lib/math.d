/++
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module lib.math;


// DSFML.
import dsfml.graphics;

// Phobos.
import std.traits;


/++
 + Creates the n-th subrectangle of proper size and position in a rectangle of given size.
 +
 + Params:
 +      n           = Index of the n-th subrect in the area.
 +      subrectSize = Size of the subrect.
 +      areaSize    = Size of the area.
 +
 + Returns:
 +      The n-th sub-rectangle.
 +/
pure nothrow @safe @nogc
Rect!T subrect(T)(uint n, Vector2!T subrectSize, Vector2!T areaSize)
if (isNumeric!T)
{
        auto pos = Vector2i(n * subrectSize.x % areaSize.x,
                            n * subrectSize.x / areaSize.x * subrectSize.y);
        return IntRect(pos, subrectSize);
}

/++
 + Keeps a value within given limits. It means that if a value is below minimum, then it's
 + set to the minimum value, same for maximum. Otherwise it is unchanged.
 +
 + Returns:
 +      The value after adjustments.
 +/
pure nothrow @safe @nogc
T clamp(T)(T value, T min, T max)
if (isComparable!T)
{
        if (value < min) return min;
        if (value > max) return max;
        return value;
}
