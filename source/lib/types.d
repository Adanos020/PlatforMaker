/++
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module lib.types;


// Unions.

///
union Int16
{
        ///
        short val;

        ///
        char[2] bytes;

        ///
        static Int16 opCall(short v)
        {
                Int16 r;
                r.val = v;
                return r;
        }

        ///
        static Int16 opCall(char[2] b)
        {
                Int16 r;
                r.bytes = b;
                return r;
        }
}

///
union Int32
{
        ///
        int val;

        ///
        char[4] bytes;

        ///
        static Int32 opCall(int v)
        {
                Int32 r;
                r.val = v;
                return r;
        }

        ///
        static Int32 opCall(char[4] b)
        {
                Int32 r;
                r.bytes = b;
                return r;
        }
}

///
union Float32
{
        ///
        float val;

        ///
        char[4] bytes;

        ///
        static Float32 opCall(float v)
        {
                Float32 r;
                r.val = v;
                return r;
        }

        ///
        static Float32 opCall(char[4] b)
        {
                Float32 r;
                r.bytes = b;
                return r;
        }
}

///
union Float64
{
        ///
        double val;

        ///
        char[8] bytes;

        ///
        static Float64 opCall(double v)
        {
                Float64 r;
                r.val = v;
                return r;
        }

        ///
        static Float64 opCall(char[8] b)
        {
                Float64 r;
                r.bytes = b;
                return r;
        }
}

///
union UColor
{
        ///
        uint    rgba;
        ///
        char[4] bytes;

        ///
        static UColor opCall(int v)
        {
                UColor r;
                r.rgba = v;
                return r;
        }

        ///
        static UColor opCall(char[4] b)
        {
                UColor r;
                r.bytes = b;
                return r;
        }
}

// Aliases.

alias TileID = ushort;
alias EntityID = ushort;


// Structures

/++
 + Stack simulation. Allows accessing only the element on top of
 + the stack, adding a new element places it on top of the stack.
 + To access the previous element the top one has to be popped out.
 +/
align(2)
struct Stack(T)
{
private: // Fields.

        T[] _payload;


public: // Methods.

        /++
         + Adds a new element to the stack.
         +
         + Params:
         +      value = New value to be added.
         +/
        nothrow
        void push(T value)
        {
                _payload ~= value;
        }

        /++
         + Removes the top element from the stack.
         +
         + Throws:
         +      InvalidMemoryOperationError if trying to pop from an empty stack.
         +/
        void pop()
        {
                static if (is (T == class) || is (T == interface))
                        destroy(_payload[$ - 1]);
                --_payload.length;
        }

        /++
         + Returns:
         +      <code>true</code> if there are no elements in the stack,
         +      <code>false</code> otherwise.
         +/
        nothrow @safe @property @nogc
        bool empty() const
        {
                return _payload.length == 0;
        }

        /++
         + Returns:
         +      Number of elements in the stack.
         +/
        nothrow @safe @property
        size_t size() const
        {
                return _payload.length;
        }

        /++
         + Params:
         +      value = The value to which the top element will be set.
         +
         + Throws:
         +      RangeError if trying to change the top element of an empty stack.
         +/
        @property
        void top(T value)
        {
                _payload[$ - 1] = value;
        }

        static if (is (T == class) || is (T == interface))
        {
                /++
                 + Returns:
                 +      The value of the top element.
                 +
                 + Throws:
                 +      RangeError if trying to access the top element of an empty stack.
                 +/
                @property
                inout(T) top() inout
                {
                        return _payload[$ - 1];
                }
        }
        else
        {
                /++
                 + ditto
                 +/
                @property
                ref inout(T) top() inout
                {
                        return _payload[$ - 1];
                }
        }
}
