/++
 + Little library containing useful functions, mainly for string mutation.
 +
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module lib.util;


// Archive.
import archive.zip;

// DSFML.
import dsfml.graphics;

// Phobos.
import std.algorithm,
       std.conv,
       std.file,
       std.json,
       std.regex,
       std.string,
       std.typecons;

debug import std.stdio;


public: // Variables.

/// List of game titles paired with corresponding path to the game archive.
Tuple!(string, string)[] gamesList;


public: // Functions.

/++
 + Removes all characters that are not allowed in file names.
 +
 + Params:
 +      str = The string to transform, without file extension.
 +
 + Returns:
 +      The string without forbidden characters.
 +/
string toFilename(string str)
{
        while (str[$ - 1] == ' ') --str.length;
        return str.tr(`/\?%*:|"<>`, "", "d");
}

/++
 + Removes the full path leaving the file name only.
 +/
string withoutPath(string path)
{
        return path[path.lastIndexOf('/') + 1 .. $];
}

/++
 + Removes extension from file name.
 +/
string withoutExtension(string path)
{
        return path[0 .. path.lastIndexOf('.')];
}

/++
 + Returns:
 +      The first integer found in a string.
 +/
int firstInt(string str)
{
        return str.matchFirst(`\-?\d+`)[0].to!int;
}

/++
 + Refreshes the list of games and paths to their files.
 +/
void listGames()
{
        gamesList = [];
        auto gameZips = dirEntries("games/", "*.zip", SpanMode.shallow);
        foreach (ref filename; gameZips)
        {
                auto zip = new ZipArchive(read(filename));
                foreach (ref member; zip.files)
                {
                        if (member.name != "info.json") continue;

                        auto info = parseJSON(cast(string) member.data);
                        gamesList ~= tuple(
                                info["name"].str,
                                cast(string) filename
                        );

                        debug writefln("Game \"%s\" from %s", info["name"].str, cast(string) filename);
                        break;
                }
        }
}
