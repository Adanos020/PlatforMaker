/++
 + Derivative of DSFML's Sprite class with support for playing animations.
 + Contains the Animation struct and the AnimatedSprite class.
 +
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module lib.animatedsprite;


// Program modules.
import engine.game.resources,
       lib.math,
       lib.types;

// DSFML.
import dsfml.graphics;

// Phobos.
import std.file,
       std.stdio;


/++
 + A set of frames, parameters and functions defining how the animation should
 + be played.
 +/
align(2)
struct Animation
{
private: // Fields.

align(1)
{
        string    _spriteSheet;
        IntRect[] _frames;
        size_t    _currentFrame;
        double    _frameTime;
        double    _timeElapsed;
        bool      _looped;
        bool      _playing;
}


public: // Methods.

        /++
         + Constructs animation from frames of any sizes.
         +
         + Params:
         +      frameTime = Duration of one frame in seconds.
         +      sheet     = ID of the texture from which the frames will be drawn.
         +      looped    = Flag telling if the animation is going to replay after
         +                  it finishes.
         +      frames    = Array of texture rectangles defining frames of the
         +                  animation.
         +/
        this(double frameTime, string sheet, IntRect[] frames, bool looped = true)
        {
                _spriteSheet = sheet;

                this._frameTime = frameTime;
                this._looped    = looped;
                this._frames    = frames;

                this._playing      = false;
                this._currentFrame = 0;
                this._timeElapsed  = 0;
        }

        /++
         + Constructs animation from frames of equal sizes.
         +
         + Params:
         +      frameTime = Duration of one frame in seconds.
         +      sheet     = ID of the texture from which the frames will be drawn.
         +      looped    = Flag telling if the animation is going to replay after
         +                  it finishes.
         +      frameSize = Size of each frame.
         +      sheetSize = Size of the spritesheet.
         +      indices   = Indices of frames in the spritesheet.
         +/
        this(double frameTime, string sheet, Vector2i frameSize, Vector2i sheetSize, int[] indices, bool looped = true)
        {
                IntRect[] frames;
                foreach (index; indices)
                {
                        frames ~= index.subrect(frameSize, sheetSize);
                }
                this(frameTime, sheet, frames, looped);
        }

        /++
         + Params:
         +      dt = Time step in seconds by which the animation will be updated.
         +
         + Returns:
         +      An IntRect being the current animation frame.
         +/
        IntRect update(double dt)
        {
                if (_playing)
                {
                        _timeElapsed += dt;

                        // If the time step is too large then at least one frame will be
                        // skipped as we don't want to slow down the animation.
                        while (_timeElapsed >= _frameTime)
                        {
                                _currentFrame = (_currentFrame + 1) % _frames.length;
                                _timeElapsed -= _frameTime;
                        }

                        if (!_looped && (_currentFrame == _frames.length - 1)) _pause();
                }

                return _frames[_currentFrame];
        }

        /++
         + Parses an array of bytes and transforms it into an Animation object.
         +
         + Params:
         +      data = Array of bytes to parse.
         +/
        bool loadFromMemory(void[] data)
        {
                // The input must contain at least 13 bytes of data:
                // 1 byte  - `looped` flag,
                // 8 bytes - frame time,
                // 4 bytes - frames count.
                if (data.length < 13) return false;

                string dataStr = cast(string) data;
                size_t currChar = 0;

                // Loading the `looped` flag.
                bool looped = cast(bool) dataStr[currChar++];

                // Loading frame time.
                Float64 ftime = [dataStr[currChar++], dataStr[currChar++], dataStr[currChar++], dataStr[currChar++],
                                 dataStr[currChar++], dataStr[currChar++], dataStr[currChar++], dataStr[currChar++]];

                // Loading frames count.
                Int32 fcount = [dataStr[currChar++], dataStr[currChar++], dataStr[currChar++], dataStr[currChar++]];

                // After knowing the number of frames we can't allow data to be less than
                // 13 + frame count * 16 bytes (as information for one frame is composed of
                // 4 4-byte integers).
                if (data.length < 13 + fcount.val * 16) return false;

                // Loading frames.
                auto frames = new IntRect[fcount.val];
                foreach (i; 0 .. fcount.val)
                {
                        // Rectangle position.
                        Int32 px = [dataStr[currChar++], dataStr[currChar++], dataStr[currChar++], dataStr[currChar++]];
                        Int32 py = [dataStr[currChar++], dataStr[currChar++], dataStr[currChar++], dataStr[currChar++]];

                        // Rectangle size.
                        Int32 sx = [dataStr[currChar++], dataStr[currChar++], dataStr[currChar++], dataStr[currChar++]];
                        Int32 sy = [dataStr[currChar++], dataStr[currChar++], dataStr[currChar++], dataStr[currChar++]];

                        // Adding the frame to the list.
                        frames[i] = IntRect(px.val, py.val, sx.val, sy.val);
                }

                // If succeeded, we can apply loaded data to the object.
                _looped = looped;
                _frameTime = ftime.val;
                _frames = frames;

                return true;
        }

        /++
         + Loads animation from file.
         +
         + Params:
         +      path = Path to file.
         +/
        bool loadFromFile(string path)
        {
                void[] data;
                try
                {
                        data = read(path);
                }
                catch (FileException ex)
                {
                        stderr.writeln("[!] " ~ ex.msg);
                        return false;
                }
                return loadFromMemory(data);
        }

        /++
         + Sets the animation to the first frame.
         +/
        void reset()
        {
                _currentFrame = 0;
        }

        void sheet(string spriteSheet) @property
        {
                _spriteSheet = spriteSheet;
        }

        const(string) sheet() const @property
        {
                return _spriteSheet;
        }

        void frames(IntRect[] fs) @property
        {
                _frames = fs;
        }

        const(IntRect[]) frames() const @property
        {
                return _frames;
        }

        void frameTime(double t) @property
        {
                _frameTime = t;
        }

        double frameTime() const @property
        {
                return _frameTime;
        }

        void fps(double rate) @property
        {
                _frameTime = 1 / rate;
        }

        double fps() const @property
        {
                return 1 / _frameTime;
        }

private: // Methods.

        void _resume()
        {
                _playing = true;
        }

        void _pause()
        {
                _playing = false;
        }

        void _start()
        {
                reset();
                _resume();
        }

        void _stop()
        {
                reset();
                _pause();
        }
}

/++
 + Derivative of DSFML's Sprite class with support for playing animations.
 +/
align(2)
class AnimatedSprite : Sprite
{
private: // Fields.

align(1)
{
        Animation[string] _animations;
        Animation*        _currentAnimation;
}


public: // Methods.

        /++
         + Constructor.
         +/
        this()
        {
                super();
                _currentAnimation = null;
        }

        /++
         + Params:
         +      id        = Key to which a new animation will be assigned.
         +      animation = New animation.
         +/
        pure nothrow @safe
        void addAnimation(string id, Animation animation)
        {
                _animations[id] = animation;
        }

        /++
         + Params:
         +      id = Name of the animation to which the sprite will switch.
         +/
        void setAnimation(string id)
        in {
                assert(id in _animations);
        }
        body {
                _currentAnimation = &_animations[id];
                _currentAnimation.reset();

                super.setTexture(Resources.texture(_currentAnimation.sheet));
        }

        /++
         + Params:
         +      dt = Time step in seconds by which the current animation will
         +           be updated.
         +/
        void update(double dt)
        {
                if (_currentAnimation is null) return;
                super.textureRect = _currentAnimation.update(dt);
        }

        /++
         + Plays the current animation from the beginning.
         +/
        void play()
        {
                _currentAnimation._start();
        }

        /++
         + Plays the animation of a given ID from the beginning.
         +
         + Params:
         +      id = Name of the animation to which the sprite will switch.
         +/
        void play(string id)
        {
                setAnimation(id);
                _currentAnimation._start();
        }

        /++
         + Stops the current animation and sets it to the beginning.
         +/
        void stop()
        {
                _currentAnimation._stop();
        }

        /++
         + Pauses the current animation.
         +/
        void pause()
        {
                _currentAnimation._pause();
        }

        /++
         + Resumes the current animation.
         +/
        void resume()
        {
                _currentAnimation._resume();
        }

        @property nothrow
        string[] animationIDs() const
        {
                return _animations.keys;
        }
}
