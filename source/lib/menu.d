/++
 + A menu is an element of the user interface allowing the user choose
 + from a range of different options in order to perform some task. In
 + this program menus will be navigated using a keyboard and game
 + controller, mouse support is planned for the future.
 +
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module lib.menu;


// Program modules.
import engine.settings,
       lib.imgui,
       lib.observer;

// DSFML.
import dsfml.graphics;

// Phobos.
import std.conv,
       std.stdio;


/++
 + ditto
 +/
align(2)
struct Menu
{
private: // Fields.

align(1)
{
        Text        _title;
        Text        _optionDisplay;
        string[][]  _options;
        Message[][] _messages;

        Vector2i _iterator = Vector2i(0, 0);

        float[]   _maxWidth; // Maximum width of an option text.
        FloatRect _optionBox;
}


public: // Methods.

        /++
         + Constructor.
         +/
        this(string title, size_t columns = 1)
        {
                _title = new Text(title, UIResources.unifont, 50);
                _title.setStyle(Text.Style.Bold);
                _title.position = Vector2f(round(Settings.Video.windowSize.x / 2 - _title.getGlobalBounds.width / 2),
                                           50);

                _optionDisplay = new Text("", UIResources.unifont, 30);
                _optionDisplay.setStyle(Text.Style.Bold);

                _optionBox = FloatRect(Settings.Video.windowSize.Vector2f / 2, Vector2f(0, 0));

                foreach (x; 0 .. columns)
                {
                        _options  ~= [""];
                        _messages ~= [Message(GameEvent.None)];
                        _maxWidth ~= 0;
                }
        }

        /++
         + Adds a new option to the menu.
         +
         + Params:
         +      option = Option text displayed in the menu.
         +      msg    = Message broadcasted after the option is chosen.
         +      column = Index of the column into which the option is going to be inserted.
         +/
        void addOption(string option, Message msg = Message(GameEvent.None), size_t column = 0)
        in {
                assert(column >= 0);
                assert(column < _options.length);
        }
        body {
                // If the first option in the column is empty then assign the new option
                // to it.
                if (_options[column][0] == "")
                {
                        _options[column][0] = option;
                        _messages[column][0] = msg;
                }
                else // Append otherwise.
                {
                        _options[column] ~= option;
                        _messages[column] ~= msg;
                }

                // After adding an option it would be nice to readjust the option box.
                _optionBox.height += 40;
                _optionBox.top = round(Settings.Video.windowSize.y / 2 - _optionBox.height / 2) + 50;

                // Checking if the new option is wider than the current maximal option
                // width in the column.
                _optionDisplay.setString("  " ~ option);
                _optionBox.width = 30;
                foreach (ref x; _maxWidth)
                {
                        if (_optionDisplay.getGlobalBounds.width > x)
                                x = _optionDisplay.getGlobalBounds.width;
                        _optionBox.width += x;
                }
                _optionBox.left = round(Settings.Video.windowSize.x / 2 - _optionBox.width / 2);
        }

        /++
         + Params:
         +      msg  = Message about an event to which the observer reacts.
         +/
        void listen(Message msg)
        {
                switch (msg.event) with (GameEvent)
                {
                case KeyboardPressedUp, JoystickMovedUp:
                        _iterator.y = (_iterator.y + cast(int) _options[_iterator.x].length - 1)
                                % cast(int) _options[_iterator.x].length;

                        debug writeln("-- Menu: Moved up.");
                        break;

                case KeyboardPressedDown, JoystickMovedDown:
                        _iterator.y = (_iterator.y + 1) % cast(int) _options[_iterator.x].length;

                        debug writeln("-- Menu: Moved down.");
                        break;

                case KeyboardPressedLeft, JoystickMovedLeft:
                        _iterator.x = (_iterator.x + cast(int) _options[_iterator.x].length - 1)
                                % cast(int) _options.length;

                        debug writeln("-- Menu: Moved left.");
                        break;

                case KeyboardPressedRight, JoystickMovedRight:
                        _iterator.x = (_iterator.x + 1) % cast(int) _options.length;

                        debug writeln("-- Menu: Moved right.");
                        break;

                case KeyboardPressedSelect, KeyboardPressedJump2, KeyboardPressedStart,
                     JoystickPressedSelect, JoystickPressedJump2, JoystickPressedStart:
                        Subject.notify(_messages[_iterator.x][_iterator.y]);

                        debug writefln(`-- Menu: Option "%s" chosen.`, _options[_iterator.x][_iterator.y]);
                        break;

                default: break;
                }
        }

        /++
         + Draws all the options to the window.
         +
         + Params:
         +      window = Render window to which the menu options will be drawn.
         +/
        void draw(RenderWindow window)
        {
                // Drawing the menu title.
                window.draw(_title);

                float xpos = 0;
                foreach (x, ref column; _options)
                {
                        foreach (y, ref option; column)
                        {
                                // Setting the display's string to the option's content.
                                _optionDisplay.setString(option);

                                // Setting the display in an appropriate position in the screen.
                                _optionDisplay.position = Vector2f(_optionBox.left + xpos,
                                                                   _optionBox.top + y * 40);

                                // If the current option is the selected one then it's going
                                // to be highlighted and an arrow will be displayed next to it.
                                if ((x == _iterator.x) && (y == _iterator.y))
                                {
                                        _optionDisplay.setString("► " ~ _optionDisplay.getString);
                                        _optionDisplay.setColor(Color.Yellow);
                                }
                                else
                                {
                                        _optionDisplay.setString("  " ~ _optionDisplay.getString);
                                        _optionDisplay.setColor(Color.White);
                                }

                                // Drawing the option.
                                window.draw(_optionDisplay);
                        }

                        // Setting the next option's x-coordinate so that it doesn't overlap
                        // with another option.
                        xpos += _maxWidth[x] + 20;
                }
        }
}
