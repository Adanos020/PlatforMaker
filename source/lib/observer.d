/++
 + Observer is a well-known design pattern used to notify some objects
 + (called Observers) about events that might be 'interesting' to them
 + and let them react to these events on their own way. Thanks to this
 + pattern objects can communicate without having any direct relations
 + like aggregation or inheritance.
 +
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module lib.observer;


// Phobos.
import std.algorithm.searching;

debug import std.stdio;


/++
 + Interface for an observer, needed to allow polymorphism. An Observer is 
 + an object that listens to messages broadcasted by the Subject and reacts
 + to them in its own way.
 +/
interface Observer
{
protected: // Methods.

        /++
         + Params:
         +      msg  = Message about an event to which the observer reacts.
         +/
        void listen(Message msg);
}

/++
 + Holds the list of all observers and notifies all of them about
 + events that might be 'interesting' to them.
 +/
struct Subject
{
private static: // Fields.

       Observer[] _observers;


public static: // Methods.

        @disable this();

        /++
         + Broadcasts a message about an event to all observers.
         +
         + Params:
         +      msg  = Message to broadcast to all observers.
         +/
        void notify(Message msg)
        {
                debug writefln(`Message "%s" with data "%s" sent.`, msg.event, msg.data);
                foreach (ref listener; _observers)
                {
                        listener.listen(msg);
                }
        }

        /++
         + Broadcasts a message about an event to all observers.
         +
         + Params:
         +      event = ID of an event that might be interesting to the observers.
         +      data  = Additional data related to the message that might be
         +              essential for the observers to react properly.
         +/
        void notify(GameEvent event, string data = "")
        {
                debug writefln(`Message "%s" with data "%s" sent.`,event,data);
                foreach (ref listener; _observers)
                {
                        listener.listen(Message(event, data));
                }
        }

        /++
         + Params:
         +      observer = Observer to add to the list.
         +/
        void add(Observer observer)
        {
                if (!_observers.canFind(observer))
                        _observers ~= observer;
        }
}

/++
 + A structure containing an ID of a GameEvent and additional data.
 +/
align(2)
struct Message
{
align(1):
        /++
         + ID of an event that might be interesting to the observers.
         +/
        GameEvent event;

        /++
         + Additional data related to the message that might be
         + essential for the observers to react properly.
         +/
        string data;
}

/++
 + List of all possible messages about events.
 +/
enum GameEvent
{
        None,

        // Game state events.
        AppExit,
        AppPopState,
        AppPushStateEditor,
        AppPushStateGame,
        AppPushStateMenu,
        AppPushStateLevel,
        AppPushStateHub,

        // Editor events.
        EditorAddEntity,
        EditorAddTile,
        EditorDeleteAudio,
        EditorDeleteEntity,
        EditorDeleteScript,
        EditorDeleteTexture,
        EditorDeleteTile,
        EditorEditEntity,
        EditorEditTile,
        EditorEntityPalette,
        EditorManageResources,
        EditorHub,
        EditorImportAudio,
        EditorImportFile,
        EditorImportScript,
        EditorImportTexture,
        EditorLevel,
        EditorPickEntity,
        EditorPickTile,
        EditorPopFrame,
        EditorProperties,
        EditorRefreshResourceList,
        EditorSelectTextureArea,
        EditorSave,
        EditorTilePalette,

        // Joystick events.
        JoystickMovedDown,
        JoystickMovedLeft,
        JoystickMovedNowhere,
        JoystickMovedRight,
        JoystickMovedUp,

        JoystickPressedAction1,
        JoystickPressedAction2,
        JoystickPressedJump1,
        JoystickPressedJump2,
        JoystickPressedRun1,
        JoystickPressedRun2,
        JoystickPressedStart,
        JoystickPressedSelect,

        JoystickReleasedAction1,
        JoystickReleasedAction2,
        JoystickReleasedJump1,
        JoystickReleasedJump2,
        JoystickReleasedRun1,
        JoystickReleasedRun2,
        JoystickReleasedStart,
        JoystickReleasedSelect,

        // Keyboard events.
        KeyboardPressedAction1,
        KeyboardPressedAction2,
        KeyboardPressedDown,
        KeyboardPressedEscape,
        KeyboardPressedJump1,
        KeyboardPressedJump2,
        KeyboardPressedLeft,
        KeyboardPressedRight,
        KeyboardPressedRun1,
        KeyboardPressedRun2,
        KeyboardPressedStart,
        KeyboardPressedSelect,
        KeyboardPressedUp,

        KeyboardReleasedAction1,
        KeyboardReleasedAction2,
        KeyboardReleasedDown,
        KeyboardReleasedEscape,
        KeyboardReleasedJump1,
        KeyboardReleasedJump2,
        KeyboardReleasedLeft,
        KeyboardReleasedRight,
        KeyboardReleasedRun1,
        KeyboardReleasedRun2,
        KeyboardReleasedStart,
        KeyboardReleasedSelect,
        KeyboardReleasedUp,

        // Menu events.
        MenuBack,
        MenuEdit,
        MenuPlay,
        MenuSettings,
}
