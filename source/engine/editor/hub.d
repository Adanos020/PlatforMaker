/++
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.editor.hub;


// Program modules.
import engine.editor.editor,
       engine.game.hub;

// DSFML.
import dsfml.graphics;


/++
 + ditto
 +/
class HubEditor : Editor
{
private: // Fields.

        Hub* _hub;

        
public: // Methods.

        /++
         +
         +/
        this()
        {
                
        }

        override void draw(RenderTarget target)
        {

        }
}
