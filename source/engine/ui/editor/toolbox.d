/++
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.ui.editor.toolbox;


// Program modules.
import engine.settings,
       lib.imgui,
       lib.observer;

// DSFML.
import dsfml.graphics;


/++
 + Bar with buttons leading to basic tools.
 +/
struct ToolBox
{
        mixin FrameData;
static:
        byte editor; // 0 - level, 1 - hub. There might appear more editors so I'm leaving this as a byte.

        /++
         +
         +/
        void initialize()
        {
                const wsize = Settings.Video.windowSize;

                frame = Frame(Vector2i(0, 0), Vector2i(wsize.x, 47),
                (RenderTarget target)
                {
                        // Exits the editor.
                        if (target.button(Vector2i(7, 7), ButtonIcon.ExitDoor, 2, "Exit."))
                        {
                                Subject.notify(GameEvent.AppPopState);
                        }

                        // Saves the game.
                        if (target.button(Vector2i(47, 7), ButtonIcon.Floppy, 2, "Save game."))
                        {
                                Subject.notify(GameEvent.EditorSave);
                        }

                        // Switches between level and map editor.
                        if (target.button(Vector2i(87, 7), ButtonIcon.Level + editor, 2,
                                        "Switch to " ~ ((editor == 1) ? "level" : "hub") ~ " editor."))
                        {
                                editor = (editor + 1) % 2;
                                if (editor == 0) Subject.notify(GameEvent.EditorLevel);
                                else             Subject.notify(GameEvent.EditorHub);           
                        }

                        // Shows a tile palette.
                        if (target.button(Vector2i(127, 7), ButtonIcon.Bricks, 2, "Tiles palette."))
                        {
                                Subject.notify(GameEvent.EditorTilePalette);
                        }

                        // Shows an entity palette.
                        if (target.button(Vector2i(167, 7), ButtonIcon.Cat, 2, "Entities palette."))
                        {
                                Subject.notify(GameEvent.EditorEntityPalette);
                        }

                        // Shows a frame for managing resources.
                        if (target.button(Vector2i(207, 7), ButtonIcon.Multimedia, 2, "Resource manager."))
                        {
                                Subject.notify(GameEvent.EditorManageResources);
                        }

                        // Shows a frame with properties of currently edited level/map and game.
                        if (target.button(Vector2i(wsize.x - 40, 7), ButtonIcon.Wrench, 2,
                                "Game and " ~ ((editor == 0) ? "level" : "hub") ~ " properties."))
                        {
                                Subject.notify(GameEvent.EditorProperties);
                        }
                });
        }

        /++
         +
         +/
        void reset()
        {
                editor = 0;
        }
}
