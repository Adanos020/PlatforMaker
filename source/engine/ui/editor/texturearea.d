/++
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.ui.editor.texturearea;


// Program modules.
import engine.game.resources,
       engine.settings,
       engine.ui.editor.tilecreator,
       lib.imgui,
       lib.observer;

// DSFML.
import dsfml.graphics;


/++
 + Texture area selector.
 +/
struct TextureArea
{
        mixin FrameData;
static:
        string texture = "";
        CroppableImageData cidata;

        /++
         +
         +/
        void initialize()
        {
                const wsize = Settings.Video.windowSize;

                frame = Frame(Vector2i(150, 32), wsize - Vector2i(300, 64),
                (RenderTarget target)
                {
                        const dpos = frame.position;
                        const dsiz = frame.size;
                        
                        target.label(Vector2i(10, 5), "Texture area selector.", 15);

                        // Texture area selecting croppable image.
                        target.croppableImage(dpos + Vector2i(10, 10), dsiz - Vector2i(20, 62),
                                              Resources.texture(TileCreator.tileSetsData.entries
                                                               [TileCreator.tileSetsData.currEntry]), cidata);

                        // Applies changes and closes the frame.
                        if (target.button(dpos + dsiz - Vector2i(84, 42), ButtonIcon.GreenTick, 2, "Apply and close."))
                        {
                                Subject.notify(GameEvent.EditorPopFrame);
                        }
                        
                        // Zoom.
                        //target.spinner();

                        // Discards changes and closes the frame.
                        if (target.button(dpos + dsiz - Vector2i(42, 42), ButtonIcon.RedCross, 2, "Cancel."))
                        {
                                Subject.notify(GameEvent.EditorPopFrame);
                        }
                });
        }

        /++
         +
         +/
        void reset()
        {
                texture = "";
                cidata.reset();
        }
}