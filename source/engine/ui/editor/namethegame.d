/++
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.ui.editor.namethegame;


// Program modules.
import engine.settings,
       engine.ui.editor.properties,
       lib.imgui,
       lib.observer;

// DSFML.
import dsfml.graphics;


/++
 + Displayed after the SAVE button is pressed but the game has no name nor path assigned.
 +/
struct NameTheGame
{
        mixin FrameData;

static:
        /++
         +
         +/
        void initialize()
        {
                const wsize = Settings.Video.windowSize;

                frame = Frame(wsize / 2 - Vector2i(200, 75), Vector2i(400, 105),
                (RenderTarget target)
                {
                        const dpos = frame.position;
                        const dsiz = frame.size;

                        target.label(Vector2i(10, 5) + dpos, "Please specify the game's name.", 20);

                        // Text field for the game's name.
                        target.textField(Vector2i(11, 35) + dpos, Properties.name, dsiz.x - 25, 30, "Name of the game.");

                        // Cancels saving.
                        if (target.button(Vector2i(dsiz.x / 2 - 87, dsiz.y - 32) + dpos, "CANCEL"))
                        {
                                Properties.name = "";
                                Subject.notify(GameEvent.EditorPopFrame);
                                return;
                        }

                        // Gives the game a name and saves the game if the text field is not empty.
                        if ((Properties.name != "") && target.button(Vector2i(dsiz.x / 2 + 16, dsiz.y - 32) + dpos, "SAVE"))
                        {
                                Subject.notify(GameEvent.EditorSave);
                        }
                });
        }
}
