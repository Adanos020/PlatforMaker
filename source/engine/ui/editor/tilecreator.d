/++
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.ui.editor.tilecreator;


// Program modules.
import engine.game.tile,
       engine.settings,
       lib.imgui,
       lib.observer;

// DSFML.
import dsfml.graphics;


/++
 + Tile creator.
 +/
struct TileCreator
{
        mixin FrameData;
static:
        /// Name of the tile currently edited.
        string tileName;

        /// Is the tile's sprite supposed to have animation?
        bool animated;

        /// Message sent to the engine after the tile is activated. Empty string means no message.
        string tileEvent;

        /// Combo box data for tile sets.
        ComboBoxData tileSetsData;

        /// Combo box data for tile collision types.
        ComboBoxData tileTypesData;

        /++
         +
         +/
        void initialize()
        {
                const wsize = Settings.Video.windowSize;

                frame = Frame(wsize / 2 - Vector2i(215, 102), Vector2i(430, 205),
                (RenderTarget target)
                {
                        const dpos = frame.position;
                        const dsiz = frame.size;

                        target.label(Vector2i(10, 5), "Tile creator.", 15);

                        // Should the tile be animated?
                        target.tickBox(Vector2i(10, 156) + dpos, "Animated", animated,
                                "Should the tile's sprite be animated?");

                        // Sprite sheets.
                        target.label(Vector2i(10, 111) + dpos, "Type", 15);
                        target.comboBox(Vector2i(10, 131) + dpos, Vector2i(dsiz.x - 174, 500), tileTypesData,
                                        "Select type.", "Selects collision type for the tile.");

                        // Sprite sheets.
                        target.label(Vector2i(10, 75) + dpos, "Tile set", 15);
                        target.comboBox(Vector2i(10, 95) + dpos, Vector2i(dsiz.x - 174, 500), tileSetsData,
                                        "Select sprite sheet.", "Selects sprite sheet for the tile.");

                        // Event.
                        target.label(Vector2i(10, 40) + dpos, "Event", 15);
                        target.textField(Vector2i(10, 60) + dpos, tileEvent, dsiz.x - 174, 15,
                                        "Message sent to the engine after the tile is activated.");

                        // Tile name, not used as an ID.
                        target.label(Vector2i(10, 5) + dpos, "Name", 15);
                        target.textField(Vector2i(10, 25) + dpos, tileName, dsiz.x - 174, 15,
                                        "Tile name, not used as an ID.");

                        if (animated)
                        {/* Animated button, TODO.
                                if (target.button(Vector2i(dsiz.x - 154, 10) + dpos, ButtonIcon.Unknown, 9, "Tile icon selector.") &&
                                tileSetsData.currEntry >= 0)
                                {

                                }*/
                        }
                        else
                        {
                                if (target.button(Vector2i(dsiz.x - 154, 10) + dpos, ButtonIcon.Unknown, 9,
                                                  "Tile icon selector.") && tileSetsData.currEntry >= 0)
                                {
                                        Subject.notify(GameEvent.EditorSelectTextureArea,
                                                       tileSetsData.entries[tileSetsData.currEntry]);
                                }
                        }

                        // Apply changes and close the frame.
                        if (tileName != "" && tileSetsData.currEntry >= 0 &&
                        target.button(dpos + dsiz - Vector2i(82, 42), ButtonIcon.GreenTick, 2, "Apply changes and close."))
                        {
                                Subject.notify(GameEvent.EditorAddTile);
                                Subject.notify(GameEvent.EditorPopFrame);
                                reset();
                        }

                        // Close the frame.
                        if (target.button(dpos + dsiz - Vector2i(42, 42), ButtonIcon.RedCross, 2, "Discard changes."))
                        {
                                Subject.notify(GameEvent.EditorPopFrame);
                                reset();
                        }
                });
        }

        /++
         +
         +/
        void reset()
        {
                tileName  = "";
                animated  = false;
                tileEvent = "";
                tileSetsData.reset();
                tileTypesData.reset();
        }

        /++
         +
         +/
        LevelTile createLevelTile()
        {
                LevelTile tile = {
                        name      : tileName,
                        type      : TileCollision.None, // TODO
                        event     : tileEvent,
                        animation : "", // TODO
                        texture   : tileSetsData.entries[tileSetsData.currEntry],
                        icon      : IntRect(0,0,0,0) // TODO
                };
                return tile;
        }
}
