/++
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.ui.editor.properties;


// Program modules.
import engine.settings,
       lib.imgui,
       lib.observer;

// DSFML.
import dsfml.graphics;


/++
 + Game properties.
 +/
struct Properties
{
        mixin FrameData;
static:
        string name;
        string author;

        /++
         +
         +/
        void initialize()
        {
                const wsize = Settings.Video.windowSize;

                frame = Frame(wsize / 2 - Vector2i(200, 87), Vector2i(400, 172),
                (RenderTarget target)
                {
                        const dpos = frame.position;
                        const dsiz = frame.size;

                        target.label(Vector2i(10, 5), "Game properties", 15);

                        // Text field for the game's name.
                        target.label(Vector2i(10, 5) + dpos, "Name", 24);
                        target.textField(Vector2i(11, 35) + dpos, name, dsiz.x - 25, 30, "Name of the game.");

                        // Text field for the game's author.
                        target.label(Vector2i(10, 65) + dpos, "Author", 24);
                        target.textField(Vector2i(11, 95) + dpos, author, dsiz.x - 25, 30, "Author of the game.");

                        // Closes the frame.
                        if (target.button(Vector2i(dsiz.x / 2 - 40, dsiz.y - 35) + dpos, " OK "))
                        {
                                Subject.notify(GameEvent.EditorPopFrame);
                                return;
                        }
                });
        }

        /++
         +
         +/
        void reset()
        {
                name   = "";
                author = "";
        }
}