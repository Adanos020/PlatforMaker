/++
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.ui.editor.resourcemanager;


// Program modules.
import engine.game.resources,
       engine.settings,
       lib.imgui,
       lib.observer;

// DSFML.
import dsfml.graphics;

// Phobos.
import std.algorithm;


/++
 + Resource management.
 +/
struct ResourceManager
{
        mixin FrameData;
static:
        ubyte         tab = 0;
        ListBoxData   resList;
        TextBlockData scriptBlockData;
        string        scriptPreview;
        string        resourceID;

        /++
         +
         +/
        void initialize()
        {
                const wsize = Settings.Video.windowSize;

                frame = Frame(Vector2i(150, 32), Vector2i(wsize.x - 300, wsize.y - 64),
                (RenderTarget target)
                {
                        const dpos = frame.position;
                        const dsiz = frame.size;

                        target.label(Vector2i(10, 5), "Game assets", 15);

                        // Handles frame tabs.
                        if (target.tabs(frame, ["TEXTURES", "AUDIO", "SCRIPTS"], tab))
                        {                        
                                switch (tab)
                                {
                                case 0:
                                        resList.entries = Resources.textureIDs;
                                        break;

                                case 1:
                                        resList.entries = Resources.audioIDs;
                                        break;

                                case 2:
                                        resList.entries = Resources.scriptIDs;
                                        break;

                                default: break;
                                }

                                resList.scrollH   = 0;
                                resList.scrollV   = 0;
                                resList.currEntry = (resList.entries.length > 0) ? 0 : -1;
                                resList.entries.sort();

                                if (resList.currEntry >= 0)
                                {
                                        resourceID = resList.entries[resList.currEntry];
                                        if (tab == 2)
                                        {
                                                scriptPreview = Resources.script(resList.entries[resList.currEntry]).data;
                                        }
                                }
                        }

                        // Closes the frame.
                        if (target.button(Vector2i(dsiz.x - 42, dsiz.y - 42) + dpos, ButtonIcon.RedCross, 2, "Close."))
                        {
                                Subject.notify(GameEvent.EditorPopFrame);
                                return;
                        }

                        // List of entries.
                        int lbwidth = dsiz.x * 2 / 5 + 24;
                        const change = target.listBox(Vector2i(10, 40) + dpos, Vector2i(lbwidth, dsiz.y - 50), resList);

                        // Resource name editor.
                        if (change)
                        {
                                resourceID = resList.entries[resList.currEntry];

                                if (tab == 2)
                                {
                                        scriptPreview = Resources.script(resList.entries[resList.currEntry]).data;
                                }
                        }
                        if (resList.entries.length > 0)
                        {
                                target.textField(Vector2i(lbwidth + 20, dsiz.y - 62) + dpos, resourceID,
                                                 dsiz.x - (lbwidth + 30), 15, "Resource name.");
                        }

                        bool thereAreEntries = (resList.currEntry >= 0);
                        bool nameIsDifferent = thereAreEntries && (resourceID != resList.entries[resList.currEntry]);

                        // Handling tabs.
                        switch (tab)
                        {
                        case 0: // Textures.
                                // Import.
                                if (target.button(Vector2i(lbwidth + 20, dsiz.y - 42) + dpos, ButtonIcon.Plus, 2,
                                                  "Import texture."))
                                {
                                        Subject.notify(GameEvent.EditorImportTexture);
                                }

                                // Delete.
                                if (thereAreEntries && target.button(Vector2i(lbwidth + 60, dsiz.y - 42) + dpos,
                                                                     ButtonIcon.Minus, 2, "Delete texture."))
                                {
                                        Subject.notify(GameEvent.EditorDeleteTexture);
                                }

                                // Apply name.
                                if (thereAreEntries && !Resources.textureExists(resourceID) &&
                                    nameIsDifferent && target.button(Vector2i(lbwidth + 100, dsiz.y - 42) + dpos,
                                                                     ButtonIcon.GreenTick, 2, "Set texture name."))
                                {
                                        Resources.renameTexture(resList.entries[resList.currEntry], resourceID);
                                        resList.entries = Resources.textureIDs;
                                        resList.entries.sort();
                                }

                                // Texture preview.
                                if (thereAreEntries)
                                {
                                        // Making sure that the image will be of correct size and have correct
                                        // aspect ratio.
                                        auto tex = Resources.texture(resList.entries[resList.currEntry]);
                                        auto targetSize = Vector2f(dsiz.x - (lbwidth + 30), dsiz.y - 118);
                                        auto texSize = tex.getSize();

                                        float scale = 1;

                                        scale *= targetSize.x / texSize.x;
                                        if (texSize.y * scale > targetSize.y)
                                        {
                                                scale *= targetSize.y / (texSize.y * scale);
                                        }

                                        // Centering the image.
                                        auto halfSize = Vector2i(cast(int) (texSize.x * scale),
                                                                 cast(int) (texSize.y * scale)) / 2;

                                        // Drawing the preview image.
                                        target.image(Vector2i(lbwidth + 20 + cast(int) targetSize.x / 2,
                                                              dsiz.y / 2 - 12) + dpos - halfSize,
                                                     Vector2f(scale, scale), tex);
                                }
                                break;

                        case 1: // Audio.
                                // Import.
                                if (target.button(Vector2i(lbwidth + 20, dsiz.y - 42) + dpos, ButtonIcon.Plus, 2,
                                                  "Import audio."))
                                {
                                        Subject.notify(GameEvent.EditorImportAudio);
                                }

                                // Delete.
                                if (thereAreEntries && target.button(Vector2i(lbwidth + 60, dsiz.y - 42) + dpos,
                                                                     ButtonIcon.Minus, 2, "Delete audio."))
                                {
                                        Subject.notify(GameEvent.EditorDeleteAudio);
                                }

                                // Apply name.
                                if (thereAreEntries && !Resources.audioExists(resourceID) && nameIsDifferent &&
                                    target.button(Vector2i(lbwidth + 100, dsiz.y - 42) + dpos, ButtonIcon.GreenTick, 2,
                                                  "Set audio name."))
                                {
                                        Resources.renameAudio(resList.entries[resList.currEntry], resourceID);
                                        resList.entries = Resources.audioIDs;
                                        resList.entries.sort();
                                }
                                break;

                        case 2: // Script.
                                // Import.
                                if (target.button(Vector2i(lbwidth + 20, dsiz.y - 42) + dpos, ButtonIcon.Plus, 2,
                                                  "Import script."))
                                {
                                        Subject.notify(GameEvent.EditorImportScript);
                                }

                                // Delete.
                                if (thereAreEntries && target.button(Vector2i(lbwidth + 60, dsiz.y - 42) + dpos,
                                                                     ButtonIcon.Minus, 2, "Delete script."))
                                {
                                        Subject.notify(GameEvent.EditorDeleteScript);
                                }

                                // Apply name.
                                if (thereAreEntries && !Resources.scriptExists(resourceID) && nameIsDifferent &&
                                    target.button(Vector2i(lbwidth + 100, dsiz.y - 42) + dpos, ButtonIcon.GreenTick, 2,
                                                  "Set script name."))
                                {
                                        Resources.renameScript(resList.entries[resList.currEntry], resourceID);
                                        resList.entries = Resources.scriptIDs;
                                        resList.entries.sort();
                                }

                                // Script preview.
                                if (thereAreEntries)
                                {
                                        target.textBlock(Vector2i(lbwidth + 20, 40) + dpos,
                                                         Vector2i(dsiz.x - 30 - lbwidth, dsiz.y - 107),
                                                         scriptPreview, 15, scriptBlockData);
                                }
                                break;

                        default: break;
                        }
                });
        }

        /++
         +
         +/
        void reset()
        {
                tab = 0;
                resList.reset();
        }
}