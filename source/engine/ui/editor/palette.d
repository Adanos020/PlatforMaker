/++
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.ui.editor.palette;


// Program modules.
import engine.game.tile,
       engine.settings,
       lib.imgui,
       lib.observer,
       lib.types,
       lib.util;

// DSFML.
import dsfml.graphics;

// Phobos.
import std.conv;


/++
 +
 +/
struct Palette
{
        mixin FrameData;
static:
        enum Type : ubyte
        {
                Tile,
                Entity
        }

        Type        type;
        ListBoxData list;
        string      thingID;

        /++
         +
         +/
        void initialize()
        {
                const wsize = Settings.Video.windowSize;

                frame = Frame(Vector2i(200, 50), Vector2i(wsize.x - 400, wsize.y - 100),
                (RenderTarget target)
                {
                        const dpos = frame.position;
                        const dsiz = frame.size;

                        target.label(Vector2i(10, 5), (type == Type.Tile) ? "Tile palette." : "Entity palette.", 15);

                        // Closes the frame.
                        if (target.button(Vector2i(dsiz.x - 42, 10) + dpos, ButtonIcon.RedCross, 2, "Close."))
                        {
                                Subject.notify(GameEvent.EditorPopFrame);
                        }

                        // List of entries.
                        const lbwidth = dsiz.x * 2 / 4 - 12;
                        target.listBox(Vector2i(10, 10) + dpos, Vector2i(lbwidth, dsiz.y - 20), list);

                        // Handling different types of palettes.
                        GameEvent eventEdit;
                        GameEvent eventDelete;
                        GameEvent eventPick;

                        string msgNew;
                        string msgDelete;
                        string msgEdit;
                        string msgPick;

                        switch (type) with (Type)
                        {
                        case Tile:
                                eventEdit   = GameEvent.EditorEditTile;
                                eventDelete = GameEvent.EditorDeleteTile;
                                eventPick   = GameEvent.EditorPickTile;
                                
                                msgNew    = "New tile.";
                                msgDelete = "Delete tile.";
                                msgEdit   = "Edit tile.";
                                msgPick   = "Pick tile.";
                                break;

                        case Entity:
                                eventEdit   = GameEvent.EditorEditEntity;
                                eventDelete = GameEvent.EditorDeleteEntity;
                                eventPick   = GameEvent.EditorPickEntity;
                                
                                msgNew    = "New entity.";
                                msgDelete = "Delete entity.";
                                msgEdit   = "Edit entity.";
                                msgPick   = "Pick entity.";
                                break;

                        // Who knows, maybe there will be some more palettes to implement?

                        default: break;
                        }

                        // Create tile/entity.
                        if (target.button(Vector2i(lbwidth + 20, dsiz.y - 42) + dpos, ButtonIcon.Plus, 2, msgNew))
                        {
                                Subject.notify(eventEdit);
                        }

                        // Delete tile/entity.
                        if ((list.currEntry >= 0) && target.button(Vector2i(lbwidth + 60, dsiz.y - 42) + dpos,
                                                                   ButtonIcon.Minus, 2, msgDelete))
                        {
                                Subject.notify(eventDelete, LevelTilePool.get(list.entries[list.currEntry]).id.to!string);
                        }

                        // Edit tile/entity.
                        if ((list.currEntry >= 0) && target.button(Vector2i(lbwidth + 100, dsiz.y - 42) + dpos,
                                                                   ButtonIcon.Pencil, 2, msgEdit))
                        {
                                Subject.notify(eventEdit, list.entries[list.currEntry]);
                        }

                        // Pick tile/entity.
                        if ((list.currEntry >= 0) && target.button(Vector2i(lbwidth + 140, dsiz.y - 42) + dpos,
                                                                   ButtonIcon.PointingHand, 2, msgPick))
                        {
                                Subject.notify(eventPick, list.entries[list.currEntry]);
                        }
                });
        }

        /++
         +
         +/
        void reset()
        {
                type    = Type.Tile;
                thingID = "";
                list.reset();
        }
}
