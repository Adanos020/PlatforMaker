/++
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.ui.editor.fileexplorer;


// Program modules.
import engine.settings,
       engine.ui.editor.resourcemanager,
       lib.imgui,
       lib.observer,
       lib.util;

// DSFML.
import dsfml.graphics;

// Phobos.
import std.algorithm,
       std.file,
       std.stdio,
       std.string;


/++
 +
 +/
struct FileExplorer
{
        mixin FrameData;
static:
        string      extensions;
        string      currPath;
        string      label;
        ListBoxData fileList;

        /++
         +
         +/
        void initialize()
        {
                const wsize = Settings.Video.windowSize;

                frame = Frame(Vector2i(150, 82), Vector2i(wsize.x - 300, wsize.y - 164),
                (RenderTarget target)
                {                                        
                        const fpos = frame.position;
                        const fsiz = frame.size;
                        
                        string[] extensions = {
                                if (ResourceManager.tab == 0) return ["bmp", "png", "tga", "psd", "hdr", "pic"];
                                if (ResourceManager.tab == 1)
                                {
                                        return ["ogg", "wav", "flac", "aiff", "au", "raw", "paf", "svx", "nist", "voc",
                                                "ircam", "w64", "mat4", "mat5", "pvf",  "htk", "sds", "avr", "sd2",
                                                "caf", "wve", "mpc2k", "rf64"];
                                }
                                if (ResourceManager.tab == 2) return ["cs"];
                                assert(0);
                        }();
                        
                        // Displaying supported resource formats.
                        target.label(Vector2i(10, 5), label, 15);

                        // Text field with the current path.
                        target.textField(Vector2i(10, 10) + fpos, currPath, fsiz.x - 22, 15, "Current path.");

                        // List of entries.
                        target.listBox(Vector2i(10, 30) + fpos, Vector2i(fsiz.x - 20, fsiz.y - 80), fileList);

                        // Going to the previous directory.
                        version (linux)
                        {
                                const bool shouldGoBack = currPath != "/home/";
                        }
                        else version (Windows) // Not tested yet.
                        {
                                import std.regex : matchAll;

                                const bool shouldGoBack = !currPath.matchAll(`^\w\:\\$`);
                        }

                        if (shouldGoBack && target.button(fsiz - Vector2i(122, 42) + fpos, ButtonIcon.FolderArrowUp, 2,
                                                        "Go to the previous directory."))
                        {
                                currPath = currPath[0 .. currPath[0 .. $ - 1].lastIndexOf('/') + 1];

                                // Resetting the indicator and scroll bar.
                                fileList.scrollH = 0;
                                fileList.scrollV = 0;
                                fileList.currEntry = 0;
                                listDirEntries(extensions);
                        }

                        // Opens the selected entry. If it's a directory, it goes to it. If it's a file,
                        // it returns the path to it and exits the file explorer.
                        if ((fileList.entries.length > 0) &&
                        target.button(fsiz - Vector2i(82, 42) + fpos, ButtonIcon.FolderPaper, 2, "Open."))
                        {
                                string path = fileList.entries[fileList.currEntry];
                                if (path[$ - 1] == '/')
                                {
                                        currPath ~= path[0 .. $];

                                        // Resetting the indicator and scroll bar.
                                        fileList.scrollH = 0;
                                        fileList.scrollV = 0;
                                        fileList.currEntry = 0;
                                        listDirEntries(extensions);
                                }
                                else
                                {
                                        // Opening the file.
                                        Subject.notify(GameEvent.EditorImportFile, currPath ~ path);
                                        
                                        // Closing the frame.
                                        Subject.notify(GameEvent.EditorPopFrame);
                                        return;
                                }
                        }

                        // Exits the file explorer.
                        if (target.button(fsiz - Vector2i(42, 42) + fpos, ButtonIcon.RedCross, 2, "Cancel."))
                        {
                                reset();
                                Subject.notify(GameEvent.EditorPopFrame);
                                return;
                        }
                });
        }

        /++
         +
         +/
        void reset()
        {
                currPath = getcwd();
                label    = "";
                fileList.reset();
        }

        /++
         +
         +/
        void listDirEntries(string[] extensionsList...)
        {
                extensions = format("(%-(%s, %))", extensionsList);

                // Clearing the list of entries.
                fileList.entries = [];
                
                DirIterator dEntries;
                try
                {
                        dEntries = dirEntries(currPath, SpanMode.shallow);
                }
                catch (Exception ex)
                {
                        stderr.writeln("[!] File explorer: " ~ ex.msg);
                        return;
                }
                foreach (entry; dEntries)
                {
                        if (entry.name.withoutPath[0] == '.') continue; // Skipping hidden files.

                        if (entry.isDir)
                        {
                                fileList.entries ~= entry.name.withoutPath ~ "/";
                        }
                        else if (extensionsList.length == 0)
                        {
                                fileList.entries ~= entry.name.withoutPath;
                        }
                        else foreach (ext; extensionsList) if (entry.name.endsWith("." ~ ext))
                        {
                                fileList.entries ~= entry.name.withoutPath;
                        }
                }
                if (fileList.entries.length > 1)
                {
                        if (fileList.entries[0] == "../")
                                fileList.entries[1 .. $].sort();
                        else
                                fileList.entries.sort();
                }
        }
}

