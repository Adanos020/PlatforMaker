/++
 + Game state implementation for editing maps and levels.
 +
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.appstate.editorstate;


// Program's modules.
import engine.appstate.appstate,
       engine.editor,
       engine.game,
       engine.settings,
       engine.ui.editor,
       lib.imgui,
       lib.observer,
       lib.types,
       lib.util;

// DSFML.
import dsfml.graphics;

// Phobos.
import std.algorithm,
       std.conv,
       std.file,
       std.string;

debug import std.stdio;


/++
 + ditto
 +/
class EditorState : AppState
{
private: // Fields.

        Stack!Frame  _frames;
        Game         _game;
        Editor[]     _editors;


public: // Methods.

        /++
         + Constructor.
         +/
        this(string gamePath = "")
        {
                // Setting default frame states.
                FileExplorer.reset();
                Properties.reset();
                ResourceManager.reset();
                ToolBox.reset();

                // Initialising frames.
                _initialize();

                // Creating and loading the game.
                _game = Game();
                if (gamePath != "") _game.load(gamePath);
                Properties.name   = _game.name;
                Properties.author = _game.author;

                // Adding game editors.
                _editors ~= new LevelEditor;
                _editors ~= new HubEditor;

                // Pushing the tool box.
                _pushFrame(ToolBox.frame);
        }

        ~this()
        {
                // Resetting frames' states.
                FileExplorer.reset();
                Properties.reset();
                ResourceManager.reset();
                ToolBox.reset();

                debug writeln("-- EditorState: Cleaned up.");
        }

        override void listen(Message msg)
        {
                switch (msg.event) with (GameEvent)
                {
                case EditorAddTile:
                {
                        debug writeln("-- EditorState: Adding tile " ~ TileCreator.tileName);
                        
                        if (ToolBox.editor == 0)
                                LevelTilePool.add(TileCreator.createLevelTile());
                        else
                                //HubTilePool.add(TileCreator.createHubTile());

                        Palette.list.entries = LevelTilePool.tileNames;
                        Palette.list.currEntry = Palette.list.entries.length > 0 ? 0 : -1;
                        
                        break;
                }

                case EditorAddEntity:
                {
                        break;
                }

                case EditorDeleteAudio:
                {
                        if (ResourceManager.resList.entries.length > 0)
                        {
                                debug writeln("-- EditorState: Deleting audio " ~
                                              ResourceManager.resList.entries[ResourceManager.resList.currEntry]);

                                // Deleting the audio file from memory.
                                Resources.unloadAudio(ResourceManager.resList.entries[ResourceManager.resList.currEntry]);

                                // Refreshing the entry list.
                                ResourceManager.resList.entries = Resources.audioIDs;

                                goto case EditorRefreshResourceList;
                        }
                        break;
                }

                case EditorDeleteScript:
                {
                        if (ResourceManager.resList.entries.length > 0)
                        {
                                debug writeln("-- EditorState: Deleting script " ~
                                              ResourceManager.resList.entries[ResourceManager.resList.currEntry]);

                                // Deleting the script file from memory.
                                Resources.unloadScript(ResourceManager.resList.entries[ResourceManager.resList.currEntry]);

                                // Refreshing the entry list.
                                ResourceManager.resList.entries = Resources.scriptIDs;

                                goto case EditorRefreshResourceList;
                        }
                        break;
                }

                case EditorDeleteTexture:
                {
                        if (ResourceManager.resList.entries.length > 0)
                        {
                                debug writeln("-- EditorState: Deleting texture " ~
                                              ResourceManager.resList.entries[ResourceManager.resList.currEntry]);

                                // Deleting the texture from memory.
                                Resources.unloadTextures(ResourceManager.resList.entries[ResourceManager.resList.currEntry]);
                                
                                // Refreshing the entry list.
                                ResourceManager.resList.entries = Resources.textureIDs;

                                goto case EditorRefreshResourceList;
                        }
                        break;
                }

                case EditorDeleteTile:
                {
                        debug writeln("-- EditorState: Removing tile " ~ msg.data);

                        assert(msg.data != "", "[!] Message data shouldn't be empty for EditorDeleteTile.");

                        if (ToolBox.editor == 0)
                                LevelTilePool.remove(msg.data.to!TileID);
                        else
                                {}

                        Palette.list.entries = LevelTilePool.tileNames;
                        Palette.list.currEntry = Palette.list.entries.length > 0 ? 0 : -1;
                        break;
                }

                case EditorEditEntity:
                {
                        break;
                }

                case EditorEditTile:
                {
                        if (msg.data != "") // Editing existing tile.
                        {
                                LevelTile tile = LevelTilePool.get(msg.data);
                                TileCreator.tileName               = tile.name;
                                TileCreator.animated               = tile.animation != "";
                                TileCreator.tileEvent              = tile.event;
                                TileCreator.tileSetsData.entries   = Resources.textureIDs;
                                TileCreator.tileSetsData.currEntry =
                                {
                                        foreach (i, texname; TileCreator.tileSetsData.entries)
                                        {
                                                if (texname == tile.texture) return cast(int) i;
                                        }
                                        return -1;
                                }();
                        }
                        else // Creating a new tile.
                        {
                                TileCreator.reset();
                                TileCreator.tileSetsData.entries = Resources.textureIDs;
                        }
                        
                        TileCreator.tileSetsData.entries.sort();
                        _pushFrame(TileCreator.frame);
                        break;
                }

                case EditorEntityPalette:
                {
                        debug writeln("-- EditorState: Entity palette frame open.");

                        _pushFrame(Palette.frame);
                        Palette.type = Palette.Type.Entity;
                        Palette.list.currEntry = (Palette.list.entries.length > 0) ? 0 : -1;
                
                        break;
                }

                case EditorHub:
                {
                        debug writeln("-- EditorState: Switched to hub editor.");
                
                        break;
                }

                case EditorImportFile:
                {
                        assert(msg.data != "", "[!] Message data shouldn't be empty for EditorImportFile.");

                        void[] data = read(msg.data);
                        string id = msg.data.withoutPath.withoutExtension;

                        switch (ResourceManager.tab)
                        {
                        case 0:
                                debug writeln("-- EditorState: Importing texture from " ~ msg.data);
                                
                                while (Resources.textureExists(id)) id ~= " (new)";
                                Resources.loadTexture(id, data);
                                ResourceManager.resList.entries = Resources.textureIDs;
                                ResourceManager.resList.entries.sort();
                                
                                break;

                        case 1:
                                debug writeln("-- EditorState: Importing audio from " ~ msg.data);

                                while (Resources.audioExists(id)) id ~= " (new)";
                                Resources.loadAudio(id, data);
                                ResourceManager.resList.entries = Resources.audioIDs;
                                ResourceManager.resList.entries.sort();
                                
                                break;

                        case 2:
                                debug writeln("-- EditorState: Importing script from " ~ msg.data);
                                
                                while (Resources.scriptExists(id)) id ~= " (new)";
                                Resources.loadScript(id, data);
                                ResourceManager.resList.entries = Resources.scriptIDs;
                                ResourceManager.resList.entries.sort();
                                
                                break;

                        default: break;
                        }
                        
                        ResourceManager.resList.currEntry = (ResourceManager.resList.entries.length > 0) ? 0 : -1;
                        ResourceManager.resourceID = ResourceManager.resList.entries[ResourceManager.resList.currEntry];
                        break;
                }

                case EditorImportAudio:
                {
                        debug writeln("-- EditorState: Audio file explorer open.");

                        _pushFrame(FileExplorer.frame);

                        FileExplorer.listDirEntries("ogg", "wav", "flac", "aiff", "au", "raw", "paf",
                                                     "svx", "nist", "voc", "ircam", "w64", "mat4", "mat5",
                                                     "pvf", "htk", "sds", "avr", "sd2", "caf", "wve", "mpc2k",
                                                     "rf64");
                        FileExplorer.label = "Import audio " ~ FileExplorer.extensions.dup.wrap.idup;
                        FileExplorer.fileList.currEntry = (FileExplorer.fileList.entries.length > 0) ? 0 : -1;
                        FileExplorer.fileList.scrollH = 0;
                        FileExplorer.fileList.scrollV = 0;

                        break;
                }

                case EditorImportScript:
                {
                        debug writeln("-- EditorState: Script file explorer open.");

                        _pushFrame(FileExplorer.frame);

                        FileExplorer.listDirEntries("cs");
                        FileExplorer.label = "Import script " ~ FileExplorer.extensions.dup.wrap.idup;
                        FileExplorer.fileList.currEntry = (FileExplorer.fileList.entries.length > 0) ? 0 : -1;
                        FileExplorer.fileList.scrollH = 0;
                        FileExplorer.fileList.scrollV = 0;

                        break;
                }
                
                case EditorImportTexture:
                {
                        debug writeln("-- EditorState: Texture file explorer open.");

                        _pushFrame(FileExplorer.frame);

                        FileExplorer.listDirEntries("bmp", "png", "tga", "psd", "hdr", "pic");
                        FileExplorer.label = "Import texture " ~ FileExplorer.extensions.dup.wrap.idup;
                        FileExplorer.fileList.currEntry = (FileExplorer.fileList.entries.length > 0) ? 0 : -1;
                        FileExplorer.fileList.scrollH = 0;
                        FileExplorer.fileList.scrollV = 0;

                        break;
                }

                case EditorLevel:
                {
                        debug writeln("-- EditorState: Switched to level editor.");
                
                        break;
                }

                case EditorManageResources:
                {
                        debug writeln("-- EditorState: Resource manager frame open.");

                        _pushFrame(ResourceManager.frame);
  
                        ResourceManager.resList.scrollH   = 0;
                        ResourceManager.resList.scrollV   = 0;
                        ResourceManager.tab              = 0;
                        ResourceManager.resList.entries   = Resources.textureIDs;
                        ResourceManager.resList.currEntry = (ResourceManager.resList.entries.length > 0) ? 0 : -1;
                        ResourceManager.resList.entries.sort();

                        if (ResourceManager.resList.currEntry >= 0)
                                ResourceManager.resourceID = ResourceManager.resList.entries[ResourceManager.resList.currEntry];

                        break;
                }

                case EditorPopFrame:
                {
                        debug writeln("-- EditorState: Top frame closed.");

                        _popFrame();
                        
                        break;
                }

                case EditorProperties:
                {
                        debug writeln("-- EditorState: Game properties frame open.");

                        _pushFrame(Properties.frame);
                        
                        break;
                }

                case EditorRefreshResourceList:
                {
                        // If we remove the last entry, we need to move the indicator to the
                        // previous one.
                        if (ResourceManager.resList.currEntry >= ResourceManager.resList.entries.length)
                                ResourceManager.resList.currEntry = cast(int) ResourceManager.resList.entries.length - 1;

                        // We don't need to operate on empty list.
                        if (ResourceManager.resList.currEntry < 0) break;

                        ResourceManager.resList.entries.sort();
                        ResourceManager.resourceID = ResourceManager.resList.entries[ResourceManager.resList.currEntry];
                
                        break;
                }

                case EditorSave:
                {
                        debug writeln("-- EditorState: Saving the game.");
                        
                        if (Properties.name != "")
                        {
                                _game.name   = Properties.name;
                                _game.author = Properties.author;
                                _game.save();
                                _popFrame();
                                listGames();

                                debug writeln("-- EditorState: Game saved.");
                        }
                        else
                        {
                                _pushFrame(NameTheGame.frame);

                                debug writeln("-- EditorState: No name save frame open.");
                        }

                        break;
                }
                
                case EditorSelectTextureArea:
                {
                        debug writeln("-- EditorState: Texture area selection frame open.");

                        assert(msg.data != "", "[!] Message data shouldn't be empty for EditorSelectTextureArea.");

                        _pushFrame(TextureArea.frame);
                        TextureArea.texture = msg.data;
                        TextureArea.cidata.reset();

                        break;
                }

                case EditorTilePalette:
                {
                        debug writeln("-- EditorState: Tile palette frame open.");

                        _pushFrame(Palette.frame);
                        Palette.type           = Palette.Type.Tile;
                        Palette.list.entries   = LevelTilePool.tileNames;
                        Palette.list.currEntry = (Palette.list.entries.length > 0) ? 0 : -1;

                        break;
                }

                default: break;
                }
        }

        override void update(double dt)
        {

        }

        override void draw(RenderWindow window)
        {
                _editors[ToolBox.editor].draw(window);
                _handleGUI(window);
        }

private: // Methods.

        ref Frame _topFrame()
        {
                return _frames.top;
        }

        void _pushFrame(Frame frame)
        {
                _frames.push(frame);
        }

        void _popFrame()
        {
                if (_frames.size > 1)
                {
                        _frames.pop();
                        UIState.keyboardItem = 0;
                }
        }

        void _handleGUI(RenderTarget target)
        {
                imguiPrepare();
                _topFrame.handle(target);
                imguiFinish();
        }

        // Here is the place where all editor frames are initialized.
        void _initialize()
        {
                FileExplorer.initialize();
                NameTheGame.initialize();
                Palette.initialize();
                Properties.initialize();
                ResourceManager.initialize();
                TextureArea.initialize();
                TileCreator.initialize();
                ToolBox.initialize();
        }
}
