/++
 + Game state implementation for the main menu.
 +
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.appstate.menustate;


// Program modules.
import engine.appstate.appstate,
       engine.settings,
       lib.menu,
       lib.observer,
       lib.types,
       lib.util;

// DSFML.
import dsfml.graphics;

// Phobos.
import std.conv;

debug import std.stdio;

/++
 + Game state implementation for the main menu.
 +/
class MenuState : AppState
{
private: // Fields.

        Stack!Menu     _menus;
        RectangleShape _background;

        
public: // Methods.

        /++
         + Constructor.
         +/
        this()
        {
                // Setting up the background.
                _background = new RectangleShape(cast(Vector2f) Settings.Video.windowSize);
                _background.fillColor = Color.Black;

                // Creating the main menu screen.
                _pushMenu(Menu("PlatforMaker"));
                _topMenu.addOption("PLAY",     Message(GameEvent.MenuPlay));
                _topMenu.addOption("EDIT",     Message(GameEvent.MenuEdit));
                _topMenu.addOption("SETTINGS", Message(GameEvent.MenuSettings));
                _topMenu.addOption("EXIT",     Message(GameEvent.AppExit));
        }

        override void listen(Message msg)
        {
                switch (msg.event) with (GameEvent)
                {
                case AppPopState:
                        _popMenu();
                        break;

                case MenuPlay:
                        _pushMenu(Menu("Choose game to play"));
                        foreach (i, game; gamesList)
                        {
                                _topMenu.addOption(game[0], Message(AppPushStateGame, i.to!string));
                        }
                        _topMenu.addOption("BACK [Esc]", Message(MenuBack));

                        debug writeln("-- MenuState: Play menu pushed.");
                        break;

                case MenuEdit:
                        _pushMenu(Menu("Choose game to edit"));
                        _topMenu.addOption("NEW GAME", Message(AppPushStateEditor));
                        foreach (i, game; gamesList)
                        {
                                _topMenu.addOption(game[0], Message(AppPushStateEditor, i.to!string));
                        }
                        _topMenu.addOption("BACK [Esc]", Message(MenuBack));

                        debug writeln("-- MenuState: Edit menu pushed.");
                        break;

                case MenuSettings:
                        _pushMenu(Menu("Settings"));
                        _topMenu.addOption("BACK [Esc]", Message(MenuBack));

                        debug writeln("-- MenuState: Settings menu pushed.");
                        break;

                case MenuBack, KeyboardPressedEscape,
                     KeyboardPressedJump1, JoystickPressedJump1:
                        if (_menus.size > 1)
                        {
                                _popMenu();
                                debug writeln("-- MenuState: Menu popped.");
                        }
                        break;

                default: break;
                }

                _topMenu.listen(msg);
        }

        override void update(double dt)
        {}

        override void draw(RenderWindow window)
        {
                window.draw(_background);
                _topMenu.draw(window);
        }

private: // Methods.

        ref Menu _topMenu()
        {
                assert(!_menus.empty);
                return _menus.top;
        }

        void _popMenu()
        {
                if (!_menus.empty)
                {
                        _menus.pop();
                }
        }

        void _pushMenu(Menu menu)
        {
                _menus.push(menu);
        }
}
