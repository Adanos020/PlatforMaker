/++
 + A game state is a part of the engine that is designed for some certain task.
 + Thanks to splitting the engine into states the code is less error prone
 + and much more tidy. It's because all states work independently on each other
 + which makes them not interfering while they shouldn't.
 +
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.appstate.appstate;


// Program modules.
import lib.observer;

// DSFML.
import dsfml.graphics;


/++
 + Abstract class allowing polymorphism for app states.
 +/
interface AppState
{
public: // Methods.

        /++
         + Params:
         +      msg  = Message about an event to which the observer reacts.
         +/
        void listen(Message msg);

        /++
         + Updates the state of content presented by the game component.
         +
         + Params:
         +      dt = Time step in seconds by which the content will be
         +           updated (used for animations and physics).
         +/
        void update(double dt);

        /++
         + Renders graphical content presented by the game component.
         +
         + Params:
         +      window = RenderWindow to which the graphical content is
         +               going to be drawn to.
         +/
        void draw(RenderWindow window);
}
