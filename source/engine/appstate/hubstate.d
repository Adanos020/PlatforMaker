/++
 + Game state implementation for navigating in a hub between levels.
 +
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.appstate.hubstate;


// Program modules.
import engine.appstate.appstate,
       engine.game.hub,
       lib.observer;

// DSFML.
import dsfml.graphics;

/++
 + ditto
 +/
class HubState : AppState
{
private: // Fields.

        Hub _hub;

        
public: // Methods.

        /++
         + Constructor.
         +/
        this()
        {
                
        }

        override void listen(Message msg)
        {

        }

        override void update(double dt)
        {

        }

        override void draw(RenderWindow window)
        {
                
        }
}
