/++
 + Handles player input either from keyboard, mouse, and gamepad.
 +
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.inputhandler;


// Program modules.
import engine.settings,
       lib.imgui,
       lib.observer;

// DSFML.
import dsfml.graphics;

// Phobos.
import std.conv,
       std.typecons;

debug import std.stdio;


private: // Variables.

align(1)
{
        Tuple!(Message, Message)[Keyboard.Key] _keyboardBindings;
        Tuple!(Message, Message)[uint] _joystickBindings;
}


public: // Functions.

/++
 + Initialises the keyboard key and gamepad button bindings.
 +/
nothrow @safe
void initInput()
{
        with (Settings.KControls) with (Keyboard.Key) with (GameEvent)
        {
                _keyboardBindings[left]    = tuple(Message(KeyboardPressedLeft),    Message(KeyboardReleasedLeft));
                _keyboardBindings[right]   = tuple(Message(KeyboardPressedRight),   Message(KeyboardReleasedRight));
                _keyboardBindings[up]      = tuple(Message(KeyboardPressedUp),      Message(KeyboardReleasedUp));
                _keyboardBindings[down]    = tuple(Message(KeyboardPressedDown),    Message(KeyboardReleasedDown));
                _keyboardBindings[run1]    = tuple(Message(KeyboardPressedRun1),    Message(KeyboardReleasedRun1));
                _keyboardBindings[run2]    = tuple(Message(KeyboardPressedRun2),    Message(KeyboardReleasedRun2));
                _keyboardBindings[jump1]   = tuple(Message(KeyboardPressedJump1),   Message(KeyboardReleasedJump1));
                _keyboardBindings[jump2]   = tuple(Message(KeyboardPressedJump2),   Message(KeyboardReleasedJump2));
                _keyboardBindings[start]   = tuple(Message(KeyboardPressedStart),   Message(KeyboardReleasedStart));
                _keyboardBindings[select]  = tuple(Message(KeyboardPressedSelect),  Message(KeyboardReleasedSelect));
                _keyboardBindings[action1] = tuple(Message(KeyboardPressedAction1), Message(KeyboardReleasedAction1));
                _keyboardBindings[action2] = tuple(Message(KeyboardPressedAction2), Message(KeyboardReleasedAction2));
                _keyboardBindings[Escape]  = tuple(Message(KeyboardPressedEscape),  Message(KeyboardReleasedEscape));
        }
        with (Settings.JControls) with (GameEvent)
        {
                _joystickBindings[run1]    = tuple(Message(JoystickPressedRun1),    Message(JoystickReleasedRun1));
                _joystickBindings[run2]    = tuple(Message(JoystickPressedRun2),    Message(JoystickReleasedRun2));
                _joystickBindings[jump1]   = tuple(Message(JoystickPressedJump1),   Message(JoystickReleasedJump1));
                _joystickBindings[jump2]   = tuple(Message(JoystickPressedJump2),   Message(JoystickReleasedJump2));
                _joystickBindings[start]   = tuple(Message(JoystickPressedStart),   Message(JoystickReleasedStart));
                _joystickBindings[select]  = tuple(Message(JoystickPressedSelect),  Message(JoystickReleasedSelect));
                _joystickBindings[action1] = tuple(Message(JoystickPressedAction1), Message(JoystickReleasedAction1));
                _joystickBindings[action2] = tuple(Message(JoystickPressedAction2), Message(JoystickReleasedAction2));
        }
}

/++
 + Params:
 +      event = Window event from which information about the key that is being
 +              pressed or released is interpreted.
 +/
void handleKeyboard(ref Event event)
{
        if (event.type == Event.EventType.TextEntered)
                UIState.keyChar = event.text.unicode;
        if ((event.type == Event.EventType.KeyPressed) && (event.key.code == Keyboard.Key.BackSpace))
                UIState.keyChar = '\b';

        UIState.keyEntered = event.key.code;

        if (event.key.code !in _keyboardBindings)
                return;

        /**/ if (event.type == Event.EventType.KeyPressed)
                Subject.notify(_keyboardBindings[event.key.code][0]);
        else if (event.type == Event.EventType.KeyReleased)
                Subject.notify(_keyboardBindings[event.key.code][1]);
}

/++
 + Params:
 +      event  = Window event from which information about the mouse is
 +               interpreted.
 +      window = Window relative to which the mouse cursor's position is
 +               determined.
 +/
void handleMouse(ref Event event, Window window)
{
        switch (event.type) with (Event.EventType)
        {
        case MouseMoved:
                UIState.mousePos = Mouse.getPosition(window);
                break;

        case MouseButtonPressed:
                /**/ if (event.mouseButton.button == Mouse.Button.Left)
                        UIState.mouseButton = 1;
                else if (event.mouseButton.button == Mouse.Button.Right)
                        UIState.mouseButton = 2;
                else if (event.mouseButton.button == Mouse.Button.Middle)
                        UIState.mouseButton = 3;
                break;

        case MouseButtonReleased:
                UIState.mouseButton = 0;
                break;

        case MouseWheelMoved:
                UIState.mouseScroll = event.mouseWheel.delta;
                break;

        default: break;
        }
}

/++
 + Params:
 +      event = Window event from which information about the mouse is
 +              interpreted.
 +/
void handleJoystick(ref Event event)
{
        if (event.joystickButton.button !in _joystickBindings) return;

        switch (event.type) with (Event.EventType)
        {
        case JoystickButtonPressed:
                Subject.notify(_joystickBindings[event.joystickButton.button][0].event,
                               event.joystickButton.joystickId.to!string);
                break;

        case JoystickButtonReleased:
                Subject.notify(_joystickBindings[event.joystickButton.button][1].event,
                               event.joystickButton.joystickId.to!string);
                break;

        case JoystickMoved:
        {
                const xpos = Joystick.getAxisPosition(0, Joystick.Axis.X);
                const ypos = Joystick.getAxisPosition(0, Joystick.Axis.Y);
                const jid  = event.joystickMove.joystickId.to!string;

                if ((xpos == 0) && (ypos == 0))
                {
                        Subject.notify(GameEvent.JoystickMovedNowhere, jid);
                        break;
                }

                /**/ if (xpos < 0) Subject.notify(GameEvent.JoystickMovedLeft,  jid);
                else if (xpos > 0) Subject.notify(GameEvent.JoystickMovedRight, jid);

                /**/ if (ypos < 0) Subject.notify(GameEvent.JoystickMovedUp,    jid);
                else if (ypos > 0) Subject.notify(GameEvent.JoystickMovedDown,  jid);

                break;
        }

        default: break;
        }
}
