/++
 + Contains game configuration data, each datum is held in a corresponding nested struct.
 +
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.settings;


// Program modules.
import engine.inputhandler;

// DSFML.
import dsfml.graphics;

// Phobos.
import std.file,
       std.json,
       std.stdio;


/++
 + Contains game configuration data, each datum is held in a corresponding nested struct.
 +/
align(2)
static struct Settings
{
static align(2):
        /++
         + Contains video configuration data.
         +/
        struct Video
        {
        static align(1):
                
                /// Current window resolution.
                Vector2i windowSize = Vector2i(800, 600);

                /// Fullscreen?
                bool fullscreen = false;

                /// Frame rate.
                enum FPS = 60.0;

                /// Frame time.
                enum FRAME_TIME = 1 / FPS;
        }

        /++
         + Contains keyboard controls.
         +/
        struct KControls
        {
        static align(1):
                /// Key for going left.
                Keyboard.Key left = Keyboard.Key.Left;

                /// Key for going right.
                Keyboard.Key right = Keyboard.Key.Right;

                /// Key for going up.
                Keyboard.Key up = Keyboard.Key.Up;

                /// Key for going down.
                Keyboard.Key down = Keyboard.Key.Down;
                
                /// Key for running.
                Keyboard.Key run1 = Keyboard.Key.A;
                
                /// Alternative key for running.
                Keyboard.Key run2 = Keyboard.Key.S;
                
                /// Key for jumping.
                Keyboard.Key jump1 = Keyboard.Key.Z;
                
                /// Alternative key for jumping.
                Keyboard.Key jump2 = Keyboard.Key.X;
                
                /// Start key.
                Keyboard.Key start = Keyboard.Key.Return;
                
                /// Select key.
                Keyboard.Key select = Keyboard.Key.RShift;
                
                /// Action key.
                Keyboard.Key action1 = Keyboard.Key.D;
                
                /// Alternative action key.
                Keyboard.Key action2 = Keyboard.Key.C;
        }

        /++
         + Contains gamepad controls.
         +/
        struct JControls
        {
        static align(1):

                /// Button for running.
                uint run1 = 3;

                /// Alternative button for running.
                uint run2 = 0;

                /// Button for jumping.
                uint jump1 = 2;

                /// Alternative button for jumping.
                uint jump2 = 1;

                /// Start button.
                uint start = 9;

                /// Select button.
                uint select = 8;

                /// Action button.
                uint action1 = 4;

                /// Alternative action button.
                uint action2 = 5;
        }

        /++
         + Loads game configuration from the config file.
         +/
        @trusted
        void load()
        {
                // Loading the JSON object.
                try
                {
                        JSONValue config = parseJSON(cast(string) read("settings.json"));

                        // Assigning the JSON object's attributes to the game config data.
                        Video.windowSize.x = cast(int)          config["resolution.x"].integer;
                        Video.windowSize.y = cast(int)          config["resolution.y"].integer;
                        Video.fullscreen   =                    config["fullscreen"  ].type == JSON_TYPE.TRUE;
                        KControls.left     = cast(Keyboard.Key) config["key.left"    ].integer;
                        KControls.right    = cast(Keyboard.Key) config["key.right"   ].integer;
                        KControls.up       = cast(Keyboard.Key) config["key.up"      ].integer;
                        KControls.down     = cast(Keyboard.Key) config["key.down"    ].integer;
                        KControls.run1     = cast(Keyboard.Key) config["key.run1"    ].integer;
                        KControls.run2     = cast(Keyboard.Key) config["key.run2"    ].integer;
                        KControls.jump1    = cast(Keyboard.Key) config["key.jump1"   ].integer;
                        KControls.jump2    = cast(Keyboard.Key) config["key.jump2"   ].integer;
                        KControls.start    = cast(Keyboard.Key) config["key.start"   ].integer;
                        KControls.select   = cast(Keyboard.Key) config["key.select"  ].integer;
                        KControls.action1  = cast(Keyboard.Key) config["key.action1" ].integer;
                        KControls.action2  = cast(Keyboard.Key) config["key.action2" ].integer;

                        if (Video.fullscreen)
                        {
                                auto vm = VideoMode.getDesktopMode();
                                Video.windowSize = Vector2i(vm.width, vm.height);
                        }
                }
                catch (Exception ex)
                {
                        stderr.writeln("[!] " ~ ex.msg ~ "\n[!] Creating a new config file.");
                        saveConfig();
                }

                // Initialising the input handler.
                initInput();
        }

        /++
         + Saves game configuration to the config file.
         +/
        @safe
        void saveConfig()
        {
                // Creating the JSON object.
                JSONValue root = [
                        "resolution.x" : JSONValue(Video.windowSize.x),
                        "resolution.y" : JSONValue(Video.windowSize.y),
                        "fullscreen"   : JSONValue(Video.fullscreen),
                        "key.left"     : JSONValue(KControls.left),
                        "key.right"    : JSONValue(KControls.right),
                        "key.up"       : JSONValue(KControls.up),
                        "key.down"     : JSONValue(KControls.down),
                        "key.run1"     : JSONValue(KControls.run1),
                        "key.run2"     : JSONValue(KControls.run2),
                        "key.jump1"    : JSONValue(KControls.jump1),
                        "key.jump2"    : JSONValue(KControls.jump2),
                        "key.start"    : JSONValue(KControls.start),
                        "key.select"   : JSONValue(KControls.select),
                        "key.action1"  : JSONValue(KControls.action1),
                        "key.action2"  : JSONValue(KControls.action2),
                        "joy.run1"     : JSONValue(JControls.run1),
                        "joy.run2"     : JSONValue(JControls.run2),
                        "joy.jump1"    : JSONValue(JControls.jump1),
                        "joy.jump2"    : JSONValue(JControls.jump2),
                        "joy.start"    : JSONValue(JControls.start),
                        "joy.select"   : JSONValue(JControls.select),
                        "joy.action1"  : JSONValue(JControls.action1),
                        "joy.action2"  : JSONValue(JControls.action2)
                ];

                // Saving the JSON object to the config file.
                File config = File("settings.json", "w");
                config.write(root.toPrettyString);

                config.close();
        }
}
