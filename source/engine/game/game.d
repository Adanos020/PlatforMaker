/++
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.game.game;


// Program modules.
import engine.game.hub,
       engine.game.level,
       engine.game.resources,
       engine.game.tile,
       lib.types,
       lib.util;

// Archive.
import archive.zip;

// DSFML.
import dsfml.graphics;

// Phobos.
import std.algorithm,
       std.conv,
       std.file,
       std.json,
       std.string;

debug import std.stdio;


/++
 +
 +/
align(2):
struct Game
{
private: // Fields.

align(1)
{
        string _name;
        string _author;
        string _path;

        Level[] _levels;
        Hub[]   _hubs;
}


public: // Methods.

        ~this()
        {
                // Unloading game resources.
                Resources.unload();
                LevelTilePool.clear();

                debug writeln("-- Game: Cleaned up.");
        }

        /++
         + Saves the game content and writes it into a zip archive.
         +/
        void save()
        {
                // Making sure that the file name is correct.
                if (_path == "") path = name;

                debug writeln("-- Game: Saving game to `" ~ _path ~ "`.");

                // Saving game info.
                debug writeln("-- Game: +-Saving game info.");
                JSONValue info = [
                        "name"   : name,
                        "author" : author
                ];

                // Constructing archive members.
                auto infoMember = new ZipArchive.File("info.json");
                infoMember.data = info.toPrettyString().dup.representation;
                infoMember.compressionMethod = CompressionMethod.deflate;

                // Members for levels.
                debug writeln("-- Game: +-Saving levels.");
                ZipArchive.File[] levelMembers;
                foreach (level; _levels)
                {
                        auto levelMember = new ZipArchive.File("levels/%s.bin".format(level.name.toFilename));
                        levelMember.data = level.save().dup.representation;
                        levelMember.compressionMethod = CompressionMethod.deflate;
                        levelMembers ~= levelMember;
                }

                // Members for level tiles.
                debug writeln("-- Game: +-Saving level tiles.");
                ZipArchive.File[] lTileMembers;
                foreach (name; LevelTilePool.tileNames)
                {
                        debug writeln("-- Game: | +-writing level_tiles/" ~ name ~ ".json");

                        const auto tile = LevelTilePool.get(name);
                        
                        // Creating a new JSON object.
                        const JSONValue tileInfo = [
                                "id"        : tile.id           .JSONValue,
                                "name"      : tile.name         .JSONValue,
                                "type"      : tile.type         .JSONValue,
                                "event"     : tile.event        .JSONValue,
                                "animation" : tile.animation    .JSONValue,
                                "texture"   : tile.texture      .JSONValue,
                                "icon"      : [
                                                tile.icon.left,
                                                tile.icon.top,
                                                tile.icon.width,
                                                tile.icon.height
                                              ]                 .JSONValue
                        ];

                        // Adding new member.
                        auto ltileMember = new ZipArchive.File("level_tiles/%s.json".format(name.toFilename));
                        ltileMember.data = tileInfo.toPrettyString.dup.representation;
                        lTileMembers ~= ltileMember;
                }

                // Members for resources.
                debug writeln("-- Game: +-Saving resources.");

                ZipArchive.File[] texMembers;
                foreach (texID; Resources.textureIDs)
                {
                        auto texMember = new ZipArchive.File("textures/%s.png".format(texID.toFilename));
                        string filename = "." ~ texID.toFilename ~ ".png"; // Hidden temporary file.

                        debug writeln("-- Game: | +-writing textures/" ~ filename[1 .. $]);
                        Resources.texture(texID).copyToImage().saveToFile(filename);
                        texMember.data = cast(immutable ubyte[]) std.file.read(filename);
                        texMember.compressionMethod = CompressionMethod.deflate;
                        texMembers ~= texMember;
 
                        std.file.remove(filename);
                }

                ZipArchive.File[] audioMembers;
                foreach (audioID; Resources.audioIDs)
                {
                        auto audioMember = new ZipArchive.File("audio/%s.ogg".format(audioID.toFilename));
                        string filename = "." ~ audioID.toFilename ~ ".ogg"; // Hidden temporary file.

                        debug writeln("-- Game: | +-writing audio/" ~ filename[1 .. $]);
                        Resources.audio(audioID).saveToFile(filename);
                        audioMember.data = cast(immutable ubyte[]) std.file.read(filename);
                        audioMember.compressionMethod = CompressionMethod.deflate;
                        audioMembers ~= audioMember;
 
                        std.file.remove(filename);
                }

                ZipArchive.File[] scriptMembers;
                foreach (scriptID; Resources.scriptIDs)
                {
                        auto scriptMember = new ZipArchive.File("scripts/%s.cs".format(scriptID.toFilename));
                        string filename = scriptID.toFilename ~ ".cs"; // Hidden temporary file.

                        debug writeln("-- Game: | +-writing scripts/" ~ filename);
                        scriptMember.data = cast(immutable ubyte[]) Resources.script(scriptID).data;
                        scriptMember.compressionMethod = CompressionMethod.deflate;
                        scriptMembers ~= scriptMember;
                }

                // Building game archive.
                debug writeln("-- Game: +-Building game archive.");
                auto zip = new ZipArchive();

                // Adding archive members.
                zip.addFile(infoMember);
                foreach (member;  levelMembers) zip.addFile(member);
                foreach (member;  lTileMembers) zip.addFile(member);
                foreach (member;    texMembers) zip.addFile(member);
                foreach (member;  audioMembers) zip.addFile(member);
                foreach (member; scriptMembers) zip.addFile(member);

                // Compressing.
                debug writeln("-- Game: +-Compressing.");
                void[] comprData = zip.serialize();

                // Writing.
                debug writeln("-- Game: +-Writing to `" ~ _path ~ "`.");
                std.file.write(_path, comprData);

                debug writeln("-- Game: Done saving.");
        }

        /++
         + Loads the game content from a zip archive.
         +
         + Params:
         +      path = Path to the archive.
         +/
        void load(string path)
        in {
                assert(path.endsWith(".zip"));
        }
        body {
                _path = path;

                debug writeln("-- Game: Opening game file " ~ path);
                auto zip = new ZipArchive(std.file.read(path));

                foreach (ref member; zip.files)
                {
                        debug writeln("-- Game: Found " ~ member.name);
                        /**/ if (member.name == "info.json")
                        {
                                debug writeln("-- Game: Loading info from " ~ member.name);

                                auto info = parseJSON(cast(string) member.data);

                                name   = info[ "name" ].str.to!string;
                                author = info["author"].str.to!string;

                                break;
                        }
                        else if (member.path.startsWith("levels/"))
                        {
                                debug writeln("-- Game: Loading level from " ~ member.name);

                                _levels ~= Level(cast(string) member.data);
                        }
                        else if (member.path.startsWith("hubs/"))
                        {
                                debug writeln("-- Game: Loading hub " ~ member.name);

                                _hubs ~= Hub(cast(string) member.data);
                        }
                        else if (member.path.startsWith("level_tiles/"))
                        {
                                debug writeln("-- Game: Loading level tile " ~ member.name);

                                JSONValue json = parseJSON(cast(string) member.data);
                                auto iconArr = cast(int[]) json["icon"].array;
                                LevelTile tile = {
                                        id        : cast(TileID)        json["id"]              .integer,
                                        name      :                     json["name"]            .str,
                                        type      : cast(TileCollision) json["type"]            .integer,
                                        event     :                     json["event"]           .str,
                                        animation :                     json["animation"]       .str,
                                        icon      : IntRect(iconArr[0], iconArr[1], iconArr[2], iconArr[2])
                                };
                                LevelTilePool.add(tile);
                        }
                        else if (member.path.startsWith("textures/"))
                        {
                                debug writeln("-- Game: Loading texture from " ~ member.name);

                                string id = member.name.withoutExtension;
                                Resources.loadTexture(id, cast(void[]) member.data);
                        }
                        else if (member.path.startsWith("audio/"))
                        {
                                debug writeln("-- Game: Loading audio from " ~ member.name);

                                string id = member.name.withoutExtension;
                                Resources.loadAudio(id, cast(void[]) member.data);
                        }
                        else if (member.path.startsWith("scripts/"))
                        {
                                debug writeln("-- Game: Loading script from " ~ member.name);

                                string id = member.name.withoutExtension;
                                Resources.loadScript(id, cast(void[]) member.data);
                        }
                }
        }

        void path(string pth) @property
        {
                _path = "games/" ~ pth.toFilename ~ ".zip";
        }

        string path() const @property
        {
                return _path;
        }

        void name(string name) @property
        {
                _name = name;
        }

        string name() const @property
        {
                return _name;
        }

        void author(string author) @property
        {
                _author = author;
        }

        string author() const @property
        {
                return _author;
        }
}
