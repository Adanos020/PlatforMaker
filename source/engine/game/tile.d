/++
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.game.tile;


// Phobos.
import std.conv,
       std.range;

// DSFML.
import dsfml.graphics;

// Program modules.
import engine.game.resources,
       lib.animatedsprite,
       lib.types;


public: // Structs.

/++
 + Set of data describing a level tile.
 +/
align(2)
struct LevelTile
{
align(1):
        /// Tile ID.
        TileID id;

        /// Tile name, not used as an ID.
        string name;

        /// Tells from what sides of the tile collision is being detected.
        TileCollision type;

        /// Event that the tile sends after being activated. Empty string meansvthat the tile doesn't
        /// trigger any event.
        string event;

        /// ID of an animation, empty string means no animation at all.
        string animation;

        /// ID of a texture with tileset.
        string texture;

        /// Index of texture fragment that will be drawn as the tile. Ignored if the tile is animated.
        IntRect icon;
}

/++
 + Pool of level tiles.
 +/
struct LevelTilePool
{
private static: // Fields.

        align(1)
        {
                Sprite                 _tileSprite;
                AnimatedSprite[TileID] _animTiles;

                LevelTile[TileID]      _tilesInfo;
                TileID[string]         _tileIndices;
        }


public static: // Methods.

        @disable this();

        /++
         + Adds a new level tile and generates a new TileID for it.
         +
         + Params:
         +      tile = The new tile to add.
         +/
        void add(LevelTile tile)
        {
                // Setting first available TileID.
                for (TileID newID = 0;; ++newID)
                {
                        if (newID !in _tilesInfo)
                        {
                                tile.id = newID;
                                break;
                        }
                }

                // Adding a new tile.
                _tilesInfo[tile.id] = tile;
                _tileIndices[tile.name] = tile.id;

                // Adding animated sprite if appropriate.
                if (tile.animation == "") return;

                auto asprite = new AnimatedSprite;

                asprite.addAnimation("default", Resources.animation(tile.animation));
                asprite.play("default");
                
                _animTiles[tile.id] = asprite;
        }

        /++
         + Removes a tile and regenerates the next tiles' IDs.
         +
         + Params:
         +      id = ID of the tile to remove.
         +/
        void remove(TileID id)
        in {
                assert(id in _tilesInfo);
        }
        body {
                // Removing the tile.
                _tileIndices.remove(_tilesInfo[id].name);
                _tilesInfo.remove(id);
        }

        /++
         + Removes a tile and regenerates the next tiles' IDs.
         +
         + Params:
         +      id = ID of the tile to remove.
         +/
        void remove(string name)
        in {
                assert(name in _tileIndices);
        }
        body {
                // Removing the tile.
                _tilesInfo.remove(_tileIndices[name]);
                _tileIndices.remove(name);
        }

        /++
         + Removes all tiles.
         +/
        void clear()
        {
                _tilesInfo.clear();
                _animTiles.clear();
                _tileIndices.clear();
        }

        /++
         + Params:
         +      id = <code>TileID</code> of the tile we want to get.
         +
         + Returns:
         +      A <code>LevelTile</code> object with the given id assigned to it.
         +/
        LevelTile get(TileID id)
        {
                return _tilesInfo[id];
        }

        /++
         + Params:
         +      name = Name of the tile we want to get.
         +
         + Returns:
         +      A <code>LevelTile</code> object with the given id assigned to it.
         +/
        LevelTile get(string name)
        {
                return get(_tileIndices[name]);
        }

        /++
         + Returns:
         +      List of names of all tiles.
         +/
        @property
        string[] tileNames()
        {
                return _tileIndices.keys;
        }

        /++
         + Updates tile animations.
         +
         + Params:
         +      dt = Time step by which the animations will be updated.
         +/
        void update(double dt)
        {
                foreach (tile; _animTiles)
                {
                        tile.update(dt);
                }
        }

        /++
         + Draws a level tile.
         +
         + Params:
         +      target = Render target to which the tile will be drawn.
         +      pos    = Position on the grid.
         +      id     = ID of the tile.
         +/
        void draw(RenderTarget target, TileID id, Vector2i pos)
        {
                if (id !in _tilesInfo) throw new Exception("[!] Tile ID " ~ id.to!string ~ " not found.");

                Sprite tile;
                if (id in _animTiles)
                {
                        tile = _animTiles[id];
                }
                else
                {
                        tile = _tileSprite;
                        tile.textureRect = _tilesInfo[id].icon;
                }

                tile.position = Vector2f(
                        pos.x * tile.getGlobalBounds().width,
                        pos.y * tile.getGlobalBounds().height
                );
                target.draw(tile);
        }
}


public: // Enums.

/++
 + Defines from what sides of the tile collision is being detected.
 +/
enum TileCollision : ubyte
{
        None   = 0,
        Bottom = 1 << 0,
        Left   = 1 << 1,
        Right  = 1 << 2,
        Top    = 1 << 3,

        Decoration = None,
        Platform   = Top,
        Solid      = Bottom | Left | Right | Top,
}
