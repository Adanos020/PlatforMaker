/++
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.game.resources;


// Program modules.
import lib.animatedsprite,
       lib.script;

// DSFML.
import dsfml.audio,
       dsfml.graphics;


/++
 + Resource manager.
 +/
struct Resources
{
private static: // Fields.

        Animation  [string] _animations;
        Script     [string] _scripts;
        SoundBuffer[string] _audio;
        Texture    [string] _textures;


public static: // Methods.

        @disable this();

        /++
         + Loads a resource from memory.
         +
         + Params:
         +      res  = Name of the resource container ("textures", "audio", or "animations").
         +      id   = Unique ID of a resource.
         +      data = Bytes of data.
         +/
        void load(string res)(string id, void[] data)
        {
                /**/ static if (res == "textures")
                {
                        Texture obj = new Texture;
                        obj.setRepeated(true);
                }
                else static if (res == "audio")
                {
                        SoundBuffer obj = new SoundBuffer;
                }
                else static if (res == "animations")
                {
                        Animation obj = Animation(0, "", []);
                }
                else static if (res == "scripts")
                {
                        Script obj = Script();
                }

                if (obj.loadFromMemory(data))
                {
                        mixin ("_" ~ res ~ "[id] = obj;");
                }
        }

        /++
         + Loads a texture from memory.
         +
         + Params:
         +      id   = Unique ID of a texture.
         +      data = Bytes of data.
         +/
        alias loadTexture = load!"textures";

        /++
         + Loads a audio file from memory.
         +
         + Params:
         +      id   = Unique ID of a audio file.
         +      data = Bytes of data.
         +/
        alias loadAudio = load!"audio";

        /++
         + Loads a script file from memory.
         +
         + Params:
         +      id   = Unique ID of a script file.
         +      data = Bytes of data.
         +/
        alias loadScript = load!"scripts";

        /++
         + Loads an animation from memory.
         +
         + Params:
         +      id   = Unique ID of an animation.
         +      data = Bytes of data.
         +/
        alias loadAnimation = load!"animations";

        /++
         + Params:
         +      res = Name of the resource container ("textures", "audio", or "animations").
         +
         + Returns:
         +      Reference to the texture of a given ID.
         +/
        auto resource(string res)(string id)
        {
                mixin("return _" ~ res ~ "[id];");
        }

        /++
         + Returns:
         +      Reference to the texture of a given ID.
         +/
        alias texture = resource!"textures";

        /++
         + Returns:
         +      Reference to the audio of a given ID.
         +/
        alias audio = resource!"audio";

        /++
         + Returns:
         +      Reference to the script of a given ID.
         +/
        alias script = resource!"scripts";

        /++
         + Returns:
         +      Reference to the sound of a given ID.
         +/
        alias animation = resource!"animations";

        /++
         + Deletes all resources.
         +
         + Params:
         +      res = Name of the resource container ("textures", "audio", or "animations").
         +      ids = List of IDs of resources to delete.
         +/
        void unload(string res = "")(string[] ids...)
        {
                static if (res == "")
                {
                        unloadTextures();
                        unloadAudio();
                        unloadAnimations();
                }
                else
                {
                        if (ids == [])
                        {
                                static if (res != "animations") foreach (obj; mixin("_" ~ res ~ ".values"))
                                {
                                        destroy(obj);
                                }
                                mixin("_" ~ res ~ ".clear();");
                        }
                        else
                        {
                                static if (res != "animations") foreach (id; ids)
                                {
                                        mixin ("destroy(_" ~ res ~ "[id]); _" ~ res ~ ".remove(id);");
                                }
                        }
                }
        }

        /++
         + Deletes all textures.
         +
         + Params:
         +      ids = List of IDs of textures to delete.
         +/
        alias unloadTextures = unload!"textures";
        
        /++
         + Deletes all audio.
         +
         + Params:
         +      ids = List of IDs of audio files to delete.
         +/
        alias unloadAudio = unload!"audio";

        /++
         + Deletes all scripts.
         +
         + Params:
         +      ids = List of IDs of scripts to delete.
         +/
        alias unloadScript = unload!"scripts";
        
        /++
         + Deletes all animations.
         +
         + Params:
         +      ids = List of IDs of animations to delete.
         +/
        alias unloadAnimations = unload!"animations";

        /++
         + Params:
         +      res = Name of the resource container ("textures", "audio", or "animations").
         +
         + Returns:
         +      IDs of all given resources stored.
         +/
        string[] resourceIDs(string res)() @property
        {
                mixin ("return _" ~ res ~ ".keys;");
        }

        /++
         + Returns:
         +      IDs of all textures stored.
         +/
        alias textureIDs = resourceIDs!"textures";

        /++
         + Returns:
         +      IDs of all audio files stored.
         +/
        alias audioIDs = resourceIDs!"audio";

        /++
         + Returns:
         +      IDs of all scripts stored.
         +/
        alias scriptIDs = resourceIDs!"scripts";

        /++
         + Returns:
         +      IDs of all animations stored.
         +/
        alias animationIDs = resourceIDs!"animations";

        /++
         + Changes a resource name.
         +
         + Params:
         +      res  = Name of the resource container ("textures", "audio", or "animations").
         +      from = Name to replace.
         +      to   = Replacement.
         +/
        void rename(string res)(string from, string to)
        in {
                mixin ("assert(from in _" ~ res ~ ");");
        }
        body {
                mixin (`_` ~ res ~ `[to] = _` ~ res ~ `[from];
                        _` ~ res ~ `.remove(from);`);
        }

        /++
         + Changes a texture name.
         +
         + Params:
         +      from = Name to replace.
         +      to   = Replacement.
         +/
        alias renameTexture = rename!"textures";

        /++
         + Changes a audio name.
         +
         + Params:
         +      from = Name to replace.
         +      to   = Replacement.
         +/
        alias renameAudio = rename!"audio";

        /++
         + Changes a script name.
         +
         + Params:
         +      from = Name to replace.
         +      to   = Replacement.
         +/
        alias renameScript = rename!"scripts";

        /++
         + Changes a animation name.
         +
         + Params:
         +      from = Name to replace.
         +      to   = Replacement.
         +/
        alias renameAnimation = rename!"animations";

        /++
         + Params:
         +      res = Name of the resource container ("textures", "audio", or "animations").
         +      id  = ID of a resource to look for.
         +
         + Returns:
         +      <code>true</code> if such an ID exists, <code>false</code> otherwise.
         +/
        bool exists(string res)(string id)
        {
                mixin ("return (id in _" ~ res ~ ") !is null;");
        }

        /++
         + Params:
         +      id = ID of a texture to look for.
         +
         + Returns:
         +      <code>true</code> if such an ID exists, <code>false</code> otherwise.
         +/
        alias textureExists = exists!"textures";

        /++
         + Params:
         +      id = ID of a audio file to look for.
         +
         + Returns:
         +      <code>true</code> if such an ID exists, <code>false</code> otherwise.
         +/
        alias audioExists = exists!"audio";

        /++
         + Params:
         +      id = ID of a script to look for.
         +
         + Returns:
         +      <code>true</code> if such an ID exists, <code>false</code> otherwise.
         +/
        alias scriptExists = exists!"scripts";

        /++
         + Params:
         +      id = ID of an animation to look for.
         +
         + Returns:
         +      <code>true</code> if such an ID exists, <code>false</code> otherwise.
         +/
        alias animationExists = exists!"animations";
}
