/++
 + Class representing set of entities in the games. Each entity is composed
 + of different components, defining it's look, behaviour and interaction
 + with other entities.
 +
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.game.entities;


// Program modules.
import lib.animatedsprite,
       lib.observer,
       lib.types;

// DSFML.
import dsfml.graphics;


/++
 + ditto
 +/
class EntitySystem : Observer
{
private: // Fields.

        GraphicsComponent[] _graphics;
        PhysicsComponent[]  _physics;


public: // Methods.

        /++
         +
         +/
        this()
        {
                Subject.add(this);
        }

        override void listen(Message msg)
        {
                foreach (ref graphics; _graphics)
                {
                        graphics.listen(msg);
                }
                foreach (ref physics; _physics)
                {
                        physics.listen(msg);
                }
        }

        /++
         +
         +/
        void update(double dt)
        {
                foreach (ref physics; _physics)
                {
                        physics.update(dt);
                }
                foreach (ref graphics; _graphics)
                {
                        graphics.update(dt);
                }
        }

        /++
         +
         +/
        void draw(RenderTarget target)
        {
                foreach (ref graphics; _graphics)
                {
                        graphics.draw(target, _physics[graphics._index]._position);
                }
        }

        /++
         +
         +/
        void addEntity(GraphicsComponent graphics,
                       PhysicsComponent  physics)
        {
                graphics._index = _graphics.length;
                physics._index  = _physics.length;

                _graphics ~= graphics;
                _physics  ~= physics;
        }
}

/++
 + Pack of data containing information where an entity of a given ID
 + should be spawned in a level.
 +/
struct EntitySpawn
{
        EntityID id;
        Vector2i position;
}

/++
 +
 +/
struct GraphicsComponent
{
private: // Fields.

        AnimatedSprite _sprite;
        ulong          _index;


public: // Methods.

        /++
         +
         +/
        void listen(Message msg)
        {
                
        }

        /++
         +
         +/
        void update(double dt)
        {
                _sprite.update(dt);
        }

        /++
         +
         +/
        void draw(RenderTarget target, Vector2f position)
        {
                _sprite.position = position;
                target.draw(_sprite);
        }

        /++
         +
         +/
        static GraphicsComponent hollow() pure @property
        {
                GraphicsComponent graphics;

                return graphics;
        }
}

/++
 +
 +/
struct PhysicsComponent
{
private: // Fields.

        Vector2f _position;
        ulong _index;


public: // Methods.

        /++
         +
         +/
        void listen(Message msg)
        {
                
        }

        /++
         +
         +/
        void update(double dt)
        {

        }

        /++
         +
         +/
        static PhysicsComponent hollow() pure @property
        {
                PhysicsComponent physics;

                return physics;
        }
}
