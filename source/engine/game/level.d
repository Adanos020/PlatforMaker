/++
 + Authors: Adam Gasior
 +
 + License:
 +      This file is part of PlatforMaker.
 + 
 +      PlatforMaker is free software: you can redistribute it and/or modify
 +      it under the terms of the GNU General Public License as published by
 +      the Free Software Foundation, either version 3 of the License, or
 +      (at your option) any later version.
 + 
 +      PlatforMaker is distributed in the hope that it will be useful,
 +      but WITHOUT ANY WARRANTY; without even the implied warranty of
 +      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 +      GNU General Public License for more details.
 + 
 +      You should have received a copy of the GNU General Public License
 +      along with PlatforMaker. If not, see <http://www.gnu.org/licenses/>.
 +/
module engine.game.level;


// Program modules.
import engine.game.entities,
       engine.game.resources,
       engine.game.tile,
       lib.animatedsprite,
       lib.types;

// DSFML.
import dsfml.graphics;

// Phobos.
import std.algorithm,
       std.conv,
       std.range;


/++
 +
 +/
align(2)
struct Level
{
private: // Fields.

align(1)
{
        string _levelID;
        string _bkgID;
        string _musicID;

        EntitySystem _entities;
        Chunk[]      _chunks;
        Vector2i     _chunkSize;
        bool         _vertical;
        Sprite       _background;
}


public: // Methods.

        /++
         + Params:
         +      name       = Level ID.
         +      background = Background texture ID.
         +      chunkSize  = Size of each chunk in tiles.
         +      vertical   = Should the chunks be stacked vertically or horizontally?
         +/
        this(string name, string background, Vector2i chunkSize, bool vertical = false)
        in {
                assert((chunkSize.x >= 16) && (chunkSize.x <= 255));
                assert((chunkSize.y >= 16) && (chunkSize.y <= 255));
        }
        body {
                this._chunkSize = chunkSize;
                this._vertical  = vertical;

                this._levelID = name;
                this._bkgID   = background;

                this._entities = new EntitySystem;

                _apply();
        }

        /++
         + Initialises the level content by loading it from memory.
         +
         + Params:
         +      data = The string to parse.
         +/
        this(string data)
        {
                load(data);
        }

        /++
         + Updates animations and entities' states.
         +
         + Params:
         +      dt = Time step by which the animations ant entities' states will be updated.
         +/
        void update(double dt)
        {
                LevelTilePool.update(dt);
                _entities.update(dt);
        }

        /++
         + Draws the level.
         +
         + Params:
         +      target = Render target to which the level will be drawn.
         +/
        void draw(RenderTarget target)
        {
                target.draw(_background);
                foreach (ref chunk; _chunks)
                {
                        chunk.draw(target);
                }
                _entities.draw(target);
        }

        /++
         + Parses a string and transforms it to level data.
         +
         + Params:
         +      data = The string to parse.
         +/
        void load(string data)
        {
                // Clearing current data.
                clear();

                size_t chIndex = 0;

                // Reading level ID.
                while (data[chIndex] != '\0')
                {
                        _levelID ~= data[chIndex++];
                }
                ++chIndex;

                // Reading background ID.
                while (data[chIndex] != '\0')
                {
                        _bkgID ~= data[chIndex++];
                }
                ++chIndex;

                // Reading music ID.
                while (data[chIndex] != '\0')
                {
                        _musicID ~= data[chIndex++];
                }
                ++chIndex;

                // Reading vertical flag.
                _vertical = cast(bool) data[chIndex++];

                // Reading chunk size;
                _chunkSize.x = cast(int) data[chIndex++];
                _chunkSize.y = cast(int) data[chIndex++];

                // Reading chunk count and initialising as much space as needed for them.
                Int16 chunkCount = [ data[chIndex++], data[chIndex++] ];
                _chunks.length = chunkCount.val;

                // Loading chunks.
                int tilesInChunk = _chunkSize.x * _chunkSize.y;
                foreach (i, ref chunk; _chunks)
                {
                        chunk = Chunk(_chunkSize, cast(int) i, _vertical);                        
                        chunk.loadTiles(data[chIndex .. chIndex + (tilesInChunk << 1)]); // Moving bits to the left gives
                        chIndex += (tilesInChunk << 1);                                  // the same result as multiplying
                }                                                                        // by powers of 2, it's just faster.

                // Loading entity spawns.
                Int16 entityCount = [ data[chIndex++], data[chIndex++] ];
                foreach (i; 0 .. entityCount.val)
                {
                        Int16 id   = [ data[chIndex++], data[chIndex++] ];
                        Int32 posx = [ data[chIndex++], data[chIndex++], data[chIndex++], data[chIndex++] ];
                        Int32 posy = [ data[chIndex++], data[chIndex++], data[chIndex++], data[chIndex++] ];

                        auto pos = Vector2i(posx.val, posy.val);
                        size_t chunk = 0;

                        // Determining entity spawn's position in the corresponding chunk.
                        if (!_vertical)
                        {
                                chunk = pos.x / _chunkSize.x;
                                pos.x %= _chunkSize.x;
                        }
                        else           
                        {
                                chunk = pos.y / _chunkSize.y;
                                pos.y %= _chunkSize.y;
                        }

                        _chunks[chunk]._entitySpawns ~= EntitySpawn(id.val, pos);
                }

                _apply();
        }

        /++
         +
         +/
        string save() const
        {
                string data;

                // Writing level ID.
                data ~= _levelID ~ '\0';

                // Writing background ID.
                data ~= _bkgID ~ '\0';

                // Writing music ID.
                data ~= _musicID ~ '\0';

                // Writing vertical flag.
                data ~= cast(char) _vertical;

                // Writing chunk size.
                data ~= cast(char) _chunkSize.x;
                data ~= cast(char) _chunkSize.y;

                // Writing chunk count.
                Int16 chunkCount = cast(short) _chunks.length;
                data ~= chunkCount.bytes;

                // Writing chunks - tile IDs and entity spawns (entity ID, pos.x, pos.y).
                foreach (ref chunk; _chunks)
                {
                        data ~= chunk.saveTiles();
                }
                foreach (ref chunk; _chunks)
                {
                        data ~= chunk.saveEntitySpawns();
                }

                return data;
        }

        /++
         + Appends a new empty chunk to the end of the list.
         +/
        void addChunk()
        {
                _chunks ~= Chunk(_chunkSize, cast(int) _chunks.length, _vertical);
        }

        /++
         + Resets all level data.
         +/
        void clear()
        {
                _levelID    = "";
                _bkgID      = "";
                _musicID    = "";
                _chunks     = [];
                _entities   = new EntitySystem;
                _vertical   = false;
                _chunkSize  = Vector2i.init;
                _background = null;
        }

        string name() const @property
        {
                return _levelID;
        }

private: // Methods.

        void _apply()
        {
                _background = new Sprite;
                _background.setTexture(Resources.texture(_bkgID));
        }
}

/++
 +
 +/
align(2)
struct Chunk
{
private: // Fields.

align(1)
{
        TileID[][]    _tileIDs;
        EntitySpawn[] _entitySpawns;
        bool          _vertical;
        int           _index;
        Vector2i      _size;
}


public: // Methods.

        /++
         +
         +/
        this(Vector2i size, int index, bool vertical)
        in {
                assert((size.x >= 16) && (size.x <= 255));
                assert((size.y >= 16) && (size.y <= 255));
        }
        body {
                _size     = size;
                _index    = index;
                _vertical = vertical;

                _tileIDs.length = size.x;
                _tileIDs[0].length = size.y;

                foreach (_; 0 .. size.x)
                {
                        foreach (__; 0 .. size.y)
                        {
                                _tileIDs[$ - 1] ~= 0;
                        }
                }
        }

        /++
         + Draws all tiles in the level to the target.
         +/
        void draw(RenderTarget target)
        {
                foreach (x, row; _tileIDs) foreach (y, id; row)
                {
                        auto pos = Vector2i(cast(int) x + cast(int) !_vertical * _index * _size.x,
                                            cast(int) y + cast(int)  _vertical * _index * _size.y);
                        LevelTilePool.draw(target, id, pos);
                }
        }

        /++
         + Parses a string and transforms it to a list of tile IDs.
         +
         + Params:
         +      data = The string to parse.
         +/
        void loadTiles(string data)
        in {
                assert(data.length % 2 == 0);
        }
        body {
                for (int i = 0; i < data.length; i += 2)
                {
                        const Int16 id = [ data[i], data[i + 1] ];
                        _tileIDs[i / _size.x][i % _size.x] = id.val;
                }
        }

        /++
         +
         +/
        string saveTiles() const
        {
                string data;

                foreach (row; _tileIDs) foreach (tile; row)
                {
                        Int16 id = tile;
                        data ~= id.bytes;
                }

                return data;
        }

        /++
         +
         +/
        string saveEntitySpawns() const
        {
                string data;

                foreach (spawn; _entitySpawns)
                {
                        Int16 id   = spawn.id;
                        Int32 posx = spawn.position.x;
                        Int32 posy = spawn.position.y;

                        data ~= id.bytes ~ posx.bytes ~ posy.bytes;
                }

                return data;
        }
}
